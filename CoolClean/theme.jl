; CoolClean/theme.jl

;; Version 0.6
;; Modified by Scott Sams <sbsams@digitallabyrinth.com>
;; This theme was modified from the CleanBig theme from Sawmill. It has support
;; for custom gradient colors. A few of the frame images were modified to look
;; more consistent.

;; Copyright (C) 1999 Red Hat, Inc., <jrb@redhat.com>

;; This theme was based on the CleanBig enlightenment theme created by
;; Michael Fulbright <drmike@redhat.com> and Carsten Haitzler
;; <raster@valinux.com>.  It is inspired by a certain, very popular
;; operating system.

(require 'gradient)

(defgroup CoolClean-frame "CoolClean Settings")

(defcustom CoolClean:gradient-type 'horizontal
  "Direction of gradient."
  :type (set horizontal vertical diagonal)
  :group CoolClean-frame
  :after-set after-setting-frame-option)

(defcustom CoolClean:solid-colors nil
  "Use solid colors instead of gradients (Uses `From' colors)."
  :group CoolClean-frame
  :type boolean
  :after-set after-setting-frame-option)

(defcustom CoolClean:normal-from-color(get-color "#8d8d8d")
  "`From' color of inactive frames."
  :type color
  :group CoolClean-frame
  :after-set after-setting-frame-option)

(defcustom CoolClean:normal-to-color (get-color "#bcbcbc")
  "`To' color of inactive frames."
  :type color
  :group CoolClean-frame
  :after-set after-setting-frame-option)

(defcustom CoolClean:active-from-color (get-color "#0100c7")
  "`From' color of active frames."
  :type color
  :group CoolClean-frame
  :after-set after-setting-frame-option)

(defcustom CoolClean:active-to-color (get-color "#00c6ef")
  "`To' color of active frames."
  :type color
  :group CoolClean-frame
  :after-set after-setting-frame-option)

(defcustom CoolClean:normal-text-color (get-color "#333333")
  "Title text color of inactive frames."
  :type color
  :group CoolClean-frame
  :after-set after-setting-frame-option)

(defcustom CoolClean:active-text-color (get-color "#FFFFFF")
  "Title text color of active frames."
  :type color
  :group CoolClean-frame
  :after-set after-setting-frame-option)

;; The font
(defcustom CoolClean:font 
  (get-font "-adobe-helvetica-bold-r-normal-*-*-120-*-*-p-*-iso8859-1")
  "Font for window titles."
  :type font
  :group CoolClean-frame
  :after-set after-setting-frame-option)



(let*
;; images
;; The standard images
;; 2x6
((bottom-images (make-image "bottom.png"))

;; 16x3
(top-images (list (make-image "top_inactive.png")
				  (make-image "top_active.png")))

;; 26x19
(title-bar-images (list (make-image "title_bar_inactive.png")
					(make-image "title_bar_active.png")))

;; 3x22
(top-right-images (make-image "top_right.png"))

;; 53x22
(top-right-button-box-images (list (make-image "top_right_button_box_inactive.png")
						   (make-image "top_right_button_box_active.png")))

;; 25x25
(bottom-left-images (make-image "bottom_left.png"))

;; 25x25
(bottom-right-images (make-image "bottom_right.png"))

;; 16x16
(close-images (list (make-image "close_normal.png")
				    nil nil
				    (make-image "close_clicked.png")))

;; 16x16
(maximize-images (list (make-image "maximize_active.png")
				       nil nil
				       (make-image "maximize_clicked.png")))

;; 16x16
(iconify-images (list (make-image "minimize_active.png")
				      nil nil
				      (make-image "minimize_clicked.png")))

;; 22x22
(menu-images (list (make-image "menu_inactive.png")
				   (make-image "menu_active.png")))

;; 6x19
(left-images (make-image "left.png"))

;; 6x19
(right-images (make-image "right.png"))

;; For the transients
;; 19x22
(t-top-right-button-box-images (list (make-image "t_top_right_button_box_inactive.png")
						     (make-image "t_top_right_button_box_active.png")))



;; For the shaded window
;; 22x22
(s-menu-images (list (make-image "s_menu_inactive.png")
				     (make-image "s_menu_active.png")))


;; 56x22
(s-top-right-images (list (make-image "s_top_right_inactive.png")
					  (make-image "s_top_right_active.png")))


;; For the Shaped, transient windows
;; 22x22
(st-top-right-images (list (make-image "st_top_right_inactive.png")
					   (make-image "st_top_right_active.png")))


(render-bg (lambda (img state)
  (apply (cond ((eq CoolClean:gradient-type 'diagonal)
        draw-diagonal-gradient)
           ((eq CoolClean:gradient-type 'horizontal)
        draw-horizontal-gradient)
           ((eq CoolClean:gradient-type 'vertical)
        draw-vertical-gradient))
         img (if state
                 (list CoolClean:active-from-color (if CoolClean:solid-colors CoolClean:active-from-color CoolClean:active-to-color))
               (list CoolClean:normal-from-color (if CoolClean:solid-colors CoolClean:normal-from-color CoolClean:normal-to-color))))
  (when (> (cdr (image-dimensions img)) 4)
    (bevel-image img 1 1))
  (set-image-border img 1 1 1 1)))


;; frame layout
(frame
  `(
    ;; title bar image
    ((background . "black")
     (top-edge . -22)
     (left-edge . 15)
     (right-edge . 49)
	 (height . 22)
     (class . top-border))

 ;; title bar
    ((renderer . ,render-bg)
     (foreground . ,(lambda () (list CoolClean:normal-text-color CoolClean:active-text-color)))
	 (font . ,(lambda () (list CoolClean:font)))
     (text . ,window-name)
     (x-justify . 4)
     (y-justify . center)
     (top-edge . -21)
	 (height . 20)
     (left-edge . 15)
     (right-edge . 49)
     (class . title))

    ;; menu button
    ((background . ,menu-images)
     (top-edge . -22)
     (left-edge . -6)
     (class . menu-button))

    ;; left border
    ((background . ,left-images)
     (left-edge . -6)
     (top-edge . 0)
     (bottom-edge . 19)
     (class . left-border))

    ;; top-right corner
    ((background . ,top-right-images)
     (right-edge . -6)
     (top-edge . -22)
     (class . top-right-corner))

    ;; top-right-button-box corner
    ((background . ,top-right-button-box-images)
     (right-edge . -3)
     (top-edge . -22)
     (class . title))

    ;; right border
    ((background . ,right-images)
     (right-edge . -6)
     (top-edge . 0)
     (bottom-edge . 0)
     (class . right-border))

    ;; bottom border
    ((background . ,bottom-images)
     (left-edge . 17)
     (right-edge . 19)
     (bottom-edge . -6)
     (class . bottom-border))

    ;; bottom-left corner
    ((background . ,bottom-left-images)
     (left-edge . -6)
     (bottom-edge . -6)
     (class . bottom-left-corner))

    ;; bottom-right corner
    ((background . ,bottom-right-images)
     (right-edge . -6)
     (bottom-edge . -6)
     (class . bottom-right-corner))

    ;; iconify button
    ((background . ,iconify-images)
     (right-edge . 31)
     (top-edge . -19)
     (class . iconify-button))

    ;; maximize button
    ((background . ,maximize-images)
     (right-edge . 14)
     (top-edge . -19)
     (class . maximize-button))

    ;; delete button
    ((background . ,close-images)
     (right-edge . -3)
     (top-edge . -19)
     (class . close-button))))


; Shaped
(shaped-frame
  `(
    ;; title bar image
    ((background . "black")
     (top-edge . -22)
     (left-edge . 15)
     (right-edge . 49)
	 (height . 22)
     (class . top-border))

	;; title bar
    ((renderer . ,render-bg)
     (foreground . ,(lambda () (list CoolClean:normal-text-color CoolClean:active-text-color)))
	 (font . ,(lambda () (list CoolClean:font)))
     (text . ,window-name)
     (x-justify . 4)
	 (y-justify . center)
     (top-edge . -21)
	 (height . 20)
     (left-edge . 15)
     (right-edge . 49)
     (class . title))

    ;; menu button
    ((background . ,s-menu-images)
     (top-edge . -22)
     (left-edge . -6)
     (class . menu-button))

    ;; top-right corner
    ((background . ,s-top-right-images)
     (right-edge . -6)
     (top-edge . -22)
     (class . title))

    ;; iconify button
    ((background . ,iconify-images)
     (right-edge . 31)
     (top-edge . -19)
     (class . iconify-button))

    ;; maximize button
    ((background . ,maximize-images)
     (right-edge . 14)
     (top-edge . -19)
     (class . maximize-button))

    ;; delete button
    ((background . ,close-images)
     (right-edge . -3)
     (top-edge . -19)
     (class . close-button))))


; Transient
(transient-frame
  `(
    ;; title bar image
    ((background . "black")
     (top-edge . -22)
     (left-edge . 15)
     (right-edge . 15)
	 (height . 22)
     (class . top-border))

	;; title bar
    ((renderer . ,render-bg)
     (foreground . ,(lambda () (list CoolClean:normal-text-color CoolClean:active-text-color)))
	 (font . ,(lambda () (list CoolClean:font)))
     (text . ,window-name)
     (x-justify . 4)
	 (y-justify . center)
     (top-edge . -21)
	 (height . 20)
     (left-edge . 15)
     (right-edge . 15)
     (class . title))

    ;; menu button
    ((background . ,menu-images)
     (top-edge . -22)
     (left-edge . -6)
     (class . menu-button))

    ;; left border
    ((background . ,left-images)
     (left-edge . -6)
     (top-edge . 0)
     (bottom-edge . 19)
     (class . left-border))

    ;; top-right corner
    ((background . ,top-right-images)
     (right-edge . -6)
     (top-edge . -22)
     (class . top-right-corner))

    ;; top-right-button-box corner
    ((background . ,t-top-right-button-box-images)
     (right-edge . -3)
     (top-edge . -22)
     (class . title))

    ;; right border
    ((background . ,right-images)
     (right-edge . -6)
     (top-edge . 0)
     (bottom-edge . 0)
     (class . right-border))

    ;; bottom border
    ((background . ,bottom-images)
     (left-edge . 17)
     (right-edge . 19)
     (bottom-edge . -6)
     (class . bottom-border))

    ;; bottom-left corner
    ((background . ,bottom-left-images)
     (left-edge . -6)
     (bottom-edge . -6)
     (class . bottom-left-corner))

    ;; bottom-right corner
    ((background . ,bottom-right-images)
     (right-edge . -6)
     (bottom-edge . -6)
     (class . bottom-right-corner))

    ;; delete button
    ((background . ,close-images)
     (right-edge . -3)
     (top-edge . -19)
     (class . close-button))))


; Shaped-transient
(shaped-transient-frame
  `(
	;; title bar image
    ((background . "black")
     (top-edge . -22)
     (left-edge . 15)
     (right-edge . 15)
	 (height . 22)
     (class . top-border))

	;; title bar
    ((renderer . ,render-bg)
     (foreground . ,(lambda () (list CoolClean:normal-text-color CoolClean:active-text-color)))
	 (font . ,(lambda () (list CoolClean:font)))
     (text . ,window-name)
     (x-justify . 4)
	 (y-justify . center)
     (top-edge . -21)
	 (height . 20)
     (left-edge . 15)
     (right-edge . 15)
     (class . title))

    ;; menu button
    ((background . ,s-menu-images)
     (top-edge . -22)
     (left-edge . -6)
     (class . menu-button))

    ;; top-right corner
    ((background . ,st-top-right-images)
     (right-edge . -6)
     (top-edge . -22)
     (class . title))

    ;; delete button
    ((background . ,close-images)
     (right-edge . -3)
     (top-edge . -19)
     (class . close-button)))))


;; Frame definition
(add-frame-style 'CoolClean
   (lambda (w type)
	 (cond ((eq type 'shaped)
		shaped-frame)
	   ((eq type 'transient)
		transient-frame)
	   ((eq type 'shaped-transient)
		shaped-transient-frame)
	   ((eq type 'unframed)
		nil-frame)
	   (t
		frame)))))
