sawfish-themes (0.13.0-1) unstable; urgency=medium

  * QA upload.
  * Orphan package.
  * Convert package to "3.0 (quilt)" format to allow using separate
    patches in debian/patches/.
    + Set pseudo upstream version string to 0.13.0.
  * Refresh packaging:
    + Bump Standards-Version to 4.6.0.
    + Bump debhelper compat to v13.
  * debian/rules: Rewrite with dh sequencer.
  * .pc: Remove hidden directory.

 -- Boyuan Yang <byang@debian.org>  Tue, 31 Aug 2021 16:32:50 -0400

sawfish-themes (0.13+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Source-only upload for testing migration.
  * debian/control: Bump Standards-Version to 4.5.1.
  * debian/control: Replace Priority: extra with Priority: optional.

 -- Boyuan Yang <byang@debian.org>  Tue, 05 Jan 2021 22:08:16 -0500

sawfish-themes (0.13) unstable; urgency=low

  * Updates to packaging format "3.0 native" -- thanks to Jari Aalto for doing
    all the legwork.   Note: dpkg does not support patches in native packages,
    so add depends to quilt and handle patching in debian/rules file.
    (Closes: #670810)
  * Update to Standards-Version to 3.9.3 and debhelper to 9.
  * Add build-arch and build-indep targets; use dh_prep in rules file.
  * Number all patches to make apply order apparent.
  * Fix copyright-refers-to-symlink-license (Lintian).
  * Fix unused-override (Lintian).

 -- Devin Carraway <devin@debian.org>  Sun, 17 Jun 2012 23:27:27 +0700

sawfish-themes (0.12) unstable; urgency=low

  * Standards-version 3.7.3
  * Lintian-clean
    + Fix override; stop installing it in the package, since the only
      override is at source level
    + Comments in dpatch files
  * Build-Depend on dpatch, debhelper (replaces Build-Depends-Indep)
  * Bourneify installation of theme changelogs (Closes: #459177)

 -- Devin Carraway <devin@debian.org>  Wed, 09 Jan 2008 23:17:01 -0800

sawfish-themes (0.11) unstable; urgency=low

  * Added CoolClean theme (Scott Sams et al); thanks to Baurjan
    Ismagulov for his help running down prior authors to obtain GPL-
    clearance.  (Closes: #195788)
  * Convert all themes (except Big) to use the wm default font or a
    generic Pango font rather than an arbitrary old-style X font
    (Closes: #203385)
  * Convert to Debian-native package versioning
  * Standards-version 3.6.1

 -- Devin Carraway <devin@debian.org>  Sun, 29 Aug 2004 01:52:00 -0700

sawfish-themes (0.10-1) unstable; urgency=low

  * New upstream version: mxflat 0.7.4
    + fixes bug whereby gaps could appear in titlebar in some button
      configurations (Closes: #188197)
  * Converted from hand-rolled patcher to dpatch

 -- Devin Carraway <devin@debian.org>  Tue, 15 Apr 2003 23:34:27 -0700

sawfish-themes (0.9-1) unstable; urgency=low

  * Added T-16 themes (Andrew Brehaut)
  * Install themes under /usr/share/sawfish/themes (Closes: #186234)
  * Added -V- (Andrew Brehaut)

 -- Devin Carraway <devin@debian.org>  Wed, 26 Mar 2003 02:35:12 -0800

sawfish-themes (0.8-1) unstable; urgency=low

  * New upstream release: mxflat 0.7.3
  * Change maintainer address to devin@debian.org

 -- Devin Carraway <devin@debian.org>  Sun, 23 Feb 2003 23:45:43 -0800

sawfish-themes (0.7-1) unstable; urgency=low

  * Added mxflat (mx & ta)
  * Added SawthenaForever (Thomas Eriksson)
  * Tidied build slightly
  * Include TODO files
  * Fix font patch for 2YearsToNever, use lucida from xfonts-*dpi

 -- Devin Carraway <debian@devin.com>  Thu, 23 Jan 2003 01:39:17 -0800

sawfish-themes (0.6-1) unstable; urgency=low

  * Added SawLook (Ricardo Lastra)
  * Added Klarth (Kenny Graunke)
  * Added Derivative (Kenny Graunke)
  * Include upstream changelogs where available
  * Rename Bubbles_Exact's READMEs to keep the translations together
  * Added absolute-metal (Andrew Midthune)
  * Added HeliX (Tuomas Kuosmanen)
  * Added DoubleHeliX (Jean-Matthieu)
  * Added Microtene (Martin Kavalec)
  * Added Titanium (Jesus Gonzales)
  * Patched Microtene to eliminate dependency on iso8859-2 font
  * Move to Standards-Version: 3.5.8

 -- Devin Carraway <debian@devin.com>  Fri,  6 Dec 2002 02:27:30 -0800

sawfish-themes (0.5-1) unstable; urgency=low

  * Initial Debianization (Closes: #164561)
  * Adjusted fonts for 2YearsToNever, BWM, Big, Blackwindow, bluefoo
    and Strap for locale-independence or to eliminate dependency on
    non-free sharefonts

 -- Devin Carraway <debian@devin.com>  Sun, 20 Oct 2002 02:24:29 -0700
