;; SawthenaForever/theme.jl
;; $Id: theme.jl,v 1.10 2001/02/11 01:33:56 jsh Exp $

;; Copyright (C) 2002 Thomas Eriksson <arne@users.sourceforge.net>
;; Copyright (C) 1999 John Harper <john@dcs.warwick.ac.uk>

;; This file were part of sawmill, I (Thomas) just did a bunch of regexp changes to 
;; make it fit my way...

;; Athena Forever is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; Athena Forever is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Athena Forever; see the file COPYING.  If not, write to
;; the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(defgroup SawthenaForever "Sawthena Forever"
  :group appearance)

(defcustom SawthenaForever:normal-color nil
  "Color of inactive frames (if unset use GTK+ background color)."
  :type (optional color)
  :group (appearance SawthenaForever)
  :user-level novice
  :after-set after-setting-frame-option)

(defcustom SawthenaForever:active-color nil
  "Color of active frames (if unset use GTK+ selection color)."
  :type (optional color)
  :group (appearance SawthenaForever)
  :user-level novice
  :after-set after-setting-frame-option)

;; more color options for text colors and such

(defcustom SawthenaForever:normal-text-color nil
  "Color of inactive title text (if unset use GTK+ background color)."
  :type (optional color)
  :group (appearance SawthenaForever)
  :user-level novice
  :after-set after-setting-frame-option)

(defcustom SawthenaForever:active-text-color nil
  "Color of active title text (if unset use GTK+ selection color)."
  :type (optional color)
  :group (appearance SawthenaForever)
  :user-level novice
  :after-set after-setting-frame-option)

;; text justification


(defcustom SawthenaForever:text-justify 'left
  "Text is \\w justified in window titles."
  :type (choice left right center)
  :group (appearance SawthenaForever)
  :after-set after-setting-frame-option)

;; 16x16

(define initialised-gtk nil)

(define (rebuild)
  (when (and (or (not SawthenaForever:normal-color)
		 (not SawthenaForever:active-color)
		 (not SawthenaForever:normal-text-color)
		 (not SawthenaForever:active-text-color))
	     (not initialised-gtk))
    (setq initialised-gtk t)
    (require 'gtkrc)
    (gtkrc-call-after-changed
     (lambda () (rebuild-frames-with-style 'SawthenaForever))))
  (rebuild-frames-with-style 'SawthenaForever))

;; frame
(define (frame-colors w)
  (list (or (window-get w 'frame-inactive-color)
	    (and (not SawthenaForever:normal-text-color)
		 (car gtkrc-background))
	    SawthenaForever:normal-color)
	(or (window-get w 'frame-active-color)
	    (and (not SawthenaForever:active-text-color)
		 (nth 3 gtkrc-background))
	    SawthenaForever:active-color)))

;; text

(define (text-colors w)
  (list (or (window-get w 'frame-inactive-color)
            (and (not SawthenaForever:normal-text-color)
                 (car gtkrc-foreground))
            SawthenaForever:normal-text-color)
        (or (window-get w 'frame-active-color)
            (and (not SawthenaForever:active-text-color)
                 (nth 3 gtkrc-foreground))
            SawthenaForever:active-text-color)))




(define (text-justifier w)
  (case SawthenaForever:text-justify
    ((left) 10)
    ((right) -10)
    ((center) 'center)))

(define frame
  `(((background . ,frame-colors)
     (foreground . ,text-colors)
     (text . ,window-name)
     (x-justify . ,text-justifier)
     (y-justify . center)
     (left-edge . 0)
     (right-edge . 0)
     (top-edge . -15)
     (height . 15)
     (class . title))

    ((background . ,frame-colors)
     (left-edge . 0)
     (right-edge . 0)
     (top-edge . -17)
     (height . 2))

    ((background . ,frame-colors)
     (left-edge . -2)
     (width . 2)
     (top-edge . -17)
     (bottom-edge . -2))

    ((background . ,frame-colors)
     (right-edge . -2)
     (width . 2)
     (top-edge . -17)
     (bottom-edge . -2))


    ((background . ,frame-colors)
     (left-edge . 0)
     (right-edge . 0)
     (bottom-edge . -2)
     (height . 2))

))
(define shaped-frame
  `(((background . ,frame-colors)
     (foreground . ,text-colors)
     (text . ,window-name)
     (x-justify . ,text-justifier)
     (y-justify . center)
     (left-edge . -2)
     (right-edge . -2)
     (top-edge . -19)
     (height . 19)
     (class . title))

))


(add-frame-style 'SawthenaForever
		 (lambda (w type)
		   (case type
		     ((default) frame)
		     ((transient) frame)
		     ((shaped) shaped-frame)
		     ((shaped-transient) shaped-frame))))

(rebuild)
(custom-set-property 'SawthenaForever:normal-color ':after-set rebuild)
