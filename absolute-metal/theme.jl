;; absolute-metal/theme.jl

;; By Andrew S. Midthune

;; This theme is heavily derived from Dr. Harper's absolute-e theme and
;; retains his copyright and the Gnu GPL license it was originally released
;; under as part of the sawfish window manager.

;; Props to John Harper for his clean and elegant code, and tigert and
;; Hallvar Helleseth for their killer graphics.

;; Copyright (C) 1999 John Harper <john@dcs.warwick.ac.uk>

;; sawmill is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; sawmill is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with sawmill; see the file COPYING.  If not, write to
;; the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;; Some of the images in this theme are by tigert, it matches the GTK theme of
;; the same name; they were originally taken from Enlightenment 0.15

;; Some of the images in this theme are from Hallvar Helleseth's
;; Absolute E theme for Enlightenment

(let*
    ((bottom-images (list (make-image "border_bottom_normal.png")
			  (make-image "border_bottom_active.png")))

     (top-images (list (make-image "border_top_normal.png")
		       (make-image "border_top_active.png")))

     (right-images (list (flip-image-diagonally
			  (copy-image (nth 0 bottom-images)))
			 (flip-image-diagonally
			  (copy-image (nth 1 bottom-images)))))

     (left-images (list (flip-image-diagonally
			 (copy-image (nth 0 top-images)))
			(flip-image-diagonally
			 (copy-image (nth 1 top-images)))))

     (top-left-images (list (make-image "corner_top_left_normal.png")
			    (make-image "corner_top_left_active.png")))

     (bottom-left-images (list (make-image "corner_bottom_left_normal.png")
			       (make-image "corner_bottom_left_active.png")))

     (top-right-images (list (make-image "corner_top_right_normal.png")
			     (make-image "corner_top_right_active.png")))

     (bottom-right-images (list (make-image "corner_bottom_right_normal.png")
				(make-image "corner_bottom_right_active.png")))

     (title-images (list (set-image-border (make-image "bar_normal.png")
					   4 4 4 4)
			 (set-image-border (make-image "bar_active.png")
					   4 4 4 4)))

     (close-images (list (set-image-border (make-image "bar_normal.png")
					   4 4 4 4)
			 (set-image-border (make-image "bar_active.png")
					   4 4 4 4)
			 (set-image-border (make-image "bar_hilited.png")
					   4 4 4 4)
			 (set-image-border (make-image "bar_clicked.png")
					   4 4 4 4)))

     (close-icons (list (make-image "close_normal.png")
			(make-image "close_active.png")))

     (frame `(((background . ,left-images)
	       (left-edge . -4)
	       (top-edge . 0)
	       (bottom-edge . 0)
	       (class . left-border))
	      ((background . ,right-images)
	       (right-edge . -4)
	       (top-edge . -15)
	       (bottom-edge . 0)
	       (class . right-border))
	      ((background . ,bottom-images)
	       (left-edge . 0)
	       (right-edge . 0)
	       (bottom-edge . -4)
	       (class . bottom-border))
	      ((background . ,top-right-images)
	       (right-edge . -4)
	       (top-edge . -19)
	       (class . top-right-corner))
	      ((background . ,bottom-left-images)
	       (left-edge . -4)
	       (bottom-edge . -4)
	       (class . bottom-left-corner))
	      ((background . ,bottom-right-images)
	       (right-edge . -4)
	       (bottom-edge . -4)
	       (class . bottom-right-corner))
	      ((background . ,title-images)
	       (foreground . ("grey" "white" "white" "white"))
	       (text . ,window-name)
	       (x-justify . center)
	       (y-justify . center)
	       (left-edge . 15)
	       (right-edge . 0)
	       (top-edge . -19)
	       (height . 19)
	       (class . title))
	      ((background . ,close-images)
	       (foreground . ,close-icons)
	       (x-justify . center)
	       (y-justify . center)
	       (left-edge . -4)
	       (top-edge . -19)
	       (width . 19)
	       (height . 19)
	       (class . close-button))))

     (shaped-frame `(((background . ,bottom-images)
		      (left-edge . 0)
		      (right-edge . 0)
		      (top-edge . 0)
		      (class . bottom-border))
		     ((background . ,right-images)
		      (right-edge . -4)
		      (top-edge . -19)
		      (height . 23)
		      (class . right-border))
		     ((background . ,top-right-images)
		      (right-edge . -4)
		      (top-edge . -19)
		      (class . top-right-corner))
		     ((background . ,bottom-left-images)
		      (left-edge . -4)
		      (top-edge . 0)
		      (class . bottom-left-corner))
		     ((background . ,bottom-right-images)
		      (right-edge . -4)
		      (top-edge . 0)
		      (class . bottom-right-corner))
		     ((background . ,title-images)
		      (foreground . ("grey" "white" "white" "white"))
		      (text . ,window-name)
		      (x-justify . center)
		      (y-justify . center)
		      (left-edge . 15)
		      (right-edge . 0)
		      (top-edge . -19)
		      (height . 19)
		      (class . title))
		     ((background . ,close-images)
		      (foreground . ,close-icons)
	       	      (x-justify . center)
	       	      (y-justify . center)
		      (left-edge . -4)
		      (top-edge . -19)
		      (width . 19)
		      (height . 19)
		      (class . close-button))))

     (transient-frame `(((background . ,top-images)
			 (left-edge . 0)
			 (right-edge . 0)
			 (top-edge . -4)
			 (class . title))
			((background . ,bottom-images)
			 (left-edge . 0)
			 (right-edge . 0)
			 (bottom-edge . -4)
			 (class . bottom-border))
			((background . ,left-images)
			 (left-edge . -4)
			 (top-edge . -4)
			 (bottom-edge . 0)
			 (class . left-border))
			((background . ,right-images)
			 (right-edge . -4)
			 (top-edge . -4)
			 (bottom-edge . 0)
			 (class . right-border))
			((background . ,top-left-images)
			 (left-edge . -4)
			 (top-edge . -4)
			 (class . top-left-corner))
			((background . ,top-right-images)
			 (right-edge . -4)
			 (top-edge . -4)
			 (class . top-right-corner))
			((background . ,bottom-left-images)
			 (left-edge . -4)
			 (bottom-edge . -4)
			 (class . bottom-left-corner))
			((background . ,bottom-right-images)
			 (right-edge . -4)
			 (bottom-edge . -4)
			 (class . bottom-right-corner))))

     (shaped-transient-frame `(((background . ,top-images)
				(left-edge . 0)
				(right-edge . 0)
				(top-edge . -4)
				(class . title))
			       ((background . ,top-left-images)
				(left-edge . -4)
				(top-edge . -4)
				(class . top-left-corner))
			       ((background . ,top-right-images)
				(right-edge . -4)
				(top-edge . -4)
				(class . top-right-corner)))))

  (add-frame-style 'absolute-metal
		   (lambda (w type)
		     (case type
		       ((default) frame)
		       ((transient) transient-frame)
		       ((shaped) shaped-frame)
		       ((shaped-transient) shaped-transient-frame)))))
