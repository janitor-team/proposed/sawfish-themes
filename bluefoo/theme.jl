;; theme file, written Mon Apr 15 21:32:21 2002
;; created by sawfish-themer -- DO NOT EDIT!

(require 'make-theme)

(let
    ((patterns-alist
      '(("border-bottom"
         (inactive
          "border-bottom.png"))
        ("border-left"
         (inactive
          "border-left.png"))
        ("border-right"
         (inactive
          "border-right.png"))
        ("icon-menu"
         (inactive
          "icon-menu.png")
         (clicked
          "icon-menu-clicked.png"))
        ("icon-close"
         (inactive
          "icon-close.png")
         (clicked
          "icon-close-clicked.png"))
        ("icon-maximize"
         (inactive
          "icon-maximize.png")
         (clicked
          "icon-maximize-clicked.png"))
        ("border-right-bottom-corner"
         (inactive
          "border-right-bottom-corner.png"))
        ("border-left-bottom-corner"
         (inactive
          "border-left-bottom-corner.png"))
        ("border-left-top-corner"
         (inactive
          "border-left-top-corner.png"))
        ("border-top"
         (inactive
          "border-top-unfocused.png")
         (focused
          "border-top.png"))
        ("border-right-top-corner"
         (inactive
          "border-right-top-corner.png"))
        ("bottom-right-corner-mod-1"
         (inactive
          "border-right-bottom-corner-mod-1.png"))
        ("border-left-bottom-shaped"
         (inactive
          "border-left-bottom-shaped.png"))
        ("border-bottom-shaped"
         (inactive
          "border-bottom-shaped.png"))
        ("border-right-bottom-shaped"
         (inactive
          "border-right-bottom-shaped.png"))))

     (frames-alist
      '(("normal"
         ((left-edge . -3)
          (right-edge . -4)
          (font . "-b&h-lucida-medium-r-normal-*-*-100-*-*-p-*-iso10646-1")
          (top-edge . -18)
          (height . 18)
          (foreground . "#ffffffffffff")
          (background . "border-top")
          (text . window-name)
          (x-justify . 16)
          (y-justify . 6)
          (class . title))
         ((left-edge . 16)
          (right-edge . 16)
          (bottom-edge . -5)
          (x-justify . center)
          (background . "border-bottom")
          (class . bottom-border))
         ((bottom-edge . 15)
          (top-edge . -1)
          (left-edge . -5)
          (background . "border-left")
          (class . left-border))
         ((bottom-edge . 15)
          (top-edge . -1)
          (right-edge . -5)
          (background . "border-right")
          (class . right-border))
         ((right-edge . -2)
          (top-edge . -18)
          (background . "icon-close")
          (class . close-button))
         ((top-edge . -18)
          (right-edge . 10)
          (background . "icon-maximize")
          (class . maximize-button))
         ((left-edge . -1)
          (top-edge . -18)
          (background . "icon-menu")
          (class . iconify-button))
         ((background . "border-right-bottom-corner")
          (right-edge . -5)
          (bottom-edge . -5)
          (class . bottom-right-corner))
         ((bottom-edge . -5)
          (left-edge . -5)
          (background . "border-left-bottom-corner")
          (class . bottom-left-corner))
         ((top-edge . -18)
          (left-edge . -5)
          (background . "border-left-top-corner")
          (class . top-left-corner))
         ((right-edge . -5)
          (top-edge . -18)
          (foreground . "border-right-top-corner")
          (background . "border-right-top-corner")
          (class . top-right-corner)))
        ("shape"
         ((text . window-name)
          (x-justify . 16)
          (font . "-b&h-lucida-medium-r-normal-*-*-100-*-*-p-*-iso10646-1")
          (right-edge . -4)
          (left-edge . -3)
          (top-edge . -18)
          (height . 18)
          (foreground . "#ffffffffffff")
          (background . "border-top")
          (y-justify . 6)
          (class . title))
         ((right-edge . -2)
          (top-edge . -18)
          (background . "icon-close")
          (class . close-button))
         ((top-edge . -18)
          (right-edge . 10)
          (background . "icon-maximize")
          (class . maximize-button))
         ((left-edge . -1)
          (top-edge . -18)
          (background . "icon-menu")
          (class . iconify-button))
         ((top-edge . -18)
          (left-edge . -5)
          (background . "border-left-top-corner")
          (class . top-left-corner))
         ((right-edge . -5)
          (top-edge . -18)
          (foreground . "border-right-top-corner")
          (background . "border-right-top-corner")
          (class . top-right-corner))
         ((top-edge . 0)
          (left-edge . -5)
          (background . "border-left-bottom-shaped")
          (class . bottom-left-corner))
         ((top-edge . 0)
          (right-edge . 0)
          (left-edge . 0)
          (background . "border-bottom-shaped")
          (class . bottom-border))
         ((background . "border-right-bottom-shaped")
          (top-edge . 0)
          (right-edge . -5)
          (class . bottom-right-corner)))))

     (mapping-alist
      '((default . "normal")
        (shaped-transient . "shape")
        (shaped . "shape")))

     (theme-name 'bluefoo))

  (add-frame-style
   theme-name (make-theme patterns-alist frames-alist mapping-alist))
  (when (boundp 'mark-frame-style-editable)
    (mark-frame-style-editable theme-name)))
