;; theme file, written Tue May 14 20:35:47 2002
;; created by sawfish-themer -- DO NOT EDIT!

;; Studio: a simply theme for sawfish window manager
;; by giangio@work
;; 
;; Created by Gianluca Sartori <gl.sartori@tiscali.it>
;; 2002-05-10


(require 'make-theme)

(let
    ((patterns-alist
      '(("top-border"
         (inactive
          "top-caption-border.png")
         (focused
          "top-caption-border.png"))
        ("left-border"
         (inactive
          "left-border.png")
         (focused
          "left-border.png"))
        ("right-border"
         (inactive
          "right-border.png")
         (focused
          "right-border.png"))
        ("bottom-border"
         (inactive
          "bottom-border.png")
         (focused
          "bottom-border.png"))
        ("bottom-left-corner"
         (inactive
          "bottom-left-corner.png")
         (focused
          "bottom-left-corner.png"))
        ("bottom-right-corner"
         (inactive
          "bottom-right-corner.png")
         (focused
          "bottom-right-corner.png"))
        ("button-close"
         (inactive
          "button-close.png")
         (focused
          "button-close.png")
         (highlighted
          "button-close-hi.png")
         (inactive-highlighted
          "button-close-hi.png"))
        ("button-minimize"
         (inactive
          "button-min.png")
         (focused
          "button-min.png")
         (highlighted
          "button-min-hi.png")
         (inactive-highlighted
          "button-min-hi.png"))
        ("button-maximize"
         (inactive
          "button-max.png")
         (focused
          "button-max.png")
         (highlighted
          "button-max-hi.png")
         (inactive-highlighted
          "button-max-hi.png"))
        ("top-bottom-border"
         (inactive
          "top-bottom-border.png")
         (focused
          "top-bottom-border.png"))
        ("top-left-corner"
         (inactive
          "top-left-corner.png")
         (focused
          "top-left-corner.png"))
        ("top-right-corner"
         (inactive
          "top-right-corner.png")
         (focused
          "top-right-corner.png"))
        ("caption"
         (inactive . "#999899989998")
         (focused
          "caption.png"
          (tiled . t)))
        ("bottons-bg"
         (inactive . "#cccccccccccc")
         (highlighted
          "caption.png"
          (tiled . t))
         (inactive-highlighted . "#999899989998"))
        ("dlg-bottom-right-corner"
         (inactive
          "dlg-bottom-right-corner.png")
         (focused
          "dlg-bottom-right-corner.png"))
        ("caption-left"
         (inactive . "#999899989998")
         (focused
          "caption-left.png"))
        ("caption-right"
         (inactive . "#999899989998")
         (focused
          "caption-right.png"))
        ("caption-bottom"
         (inactive . "#999899989998")
         (focused . "#8ccc9709a8f5"))
        ("caption-top"
         (inactive . "#999899989998")
         (focused . "#733275c287ad"))))

     (frames-alist
      '(("default"
         ((height . 3)
          (top-edge . -17)
          (right-edge . -1)
          (left-edge . -1)
          (background . "top-border")
          (class . top-border))
         ((top-edge . -14)
          (height . 11)
          (left-edge . -1)
          (right-edge . -1)
          (background . "#cccccccccccc"))
         ((height . 3)
          (top-edge . -3)
          (left-edge . -1)
          (background . "top-bottom-border")
          (right-edge . -1))
         ((top-edge . -1)
          (background . "left-border")
          (bottom-edge . 12)
          (left-edge . -4)
          (class . left-border))
         ((left-edge . -4)
          (height . 16)
          (background . "top-left-corner")
          (top-edge . -17)
          (class . top-left-corner))
         ((background . "top-right-corner")
          (height . 16)
          (top-edge . -17)
          (right-edge . -4)
          (class . top-right-corner))
         ((background . "left-border")
          (bottom-edge . 12)
          (top-edge . -1)
          (right-edge . -4)
          (class . right-border))
         ((background . "bottom-left-corner")
          (bottom-edge . -4)
          (left-edge . -4)
          (class . bottom-left-corner))
         ((background . "bottom-right-corner")
          (bottom-edge . -4)
          (right-edge . -4)
          (class . bottom-right-corner))
         ((left-edge . 12)
          (right-edge . 12)
          (background . "bottom-border")
          (bottom-edge . -4)
          (class . bottom-border))
         ((width . 12)
          (left-edge . -1)
          (top-edge . -14)
          (height . 11)
          (background . "bottons-bg")
          (foreground . "button-close")
          (class . close-button))
         ((right-edge . 13)
          (background . "bottons-bg")
          (foreground . "button-minimize")
          (height . 11)
          (width . 12)
          (top-edge . -14)
          (class . iconify-button))
         ((right-edge . 0)
          (foreground . "button-maximize")
          (height . 11)
          (width . 12)
          (background . "bottons-bg")
          (top-edge . -14)
          (class . maximize-button))
         ((font . "-schumacher-clean-medium-r-normal-*-*-120-*-*-c-*-iso646.1991-irv")
          (height . 11)
          (top-edge . -14)
          (left-edge . 12)
          (right-edge . 26)
          (background . "caption")
          (y-justify . center)
          (x-justify . center)
          (text . window-name)
          (foreground . "#ffffffffffff")
          (class . title)))
        ("shaded"
         ((height . 3)
          (top-edge . -17)
          (left-edge . -1)
          (right-edge . -1)
          (background . "top-border")
          (class . top-border))
         ((top-edge . -14)
          (height . 11)
          (left-edge . -1)
          (right-edge . -1)
          (background . "#cccccccccccc"))
         ((top-edge . -3)
          (height . 3)
          (left-edge . -1)
          (background . "top-bottom-border")
          (right-edge . -1))
         ((top-edge . -17)
          (left-edge . -4)
          (height . 16)
          (background . "top-left-corner")
          (class . top-left-corner))
         ((top-edge . -17)
          (height . 16)
          (background . "top-right-corner")
          (right-edge . -4)
          (class . top-right-corner))
         ((left-edge . -1)
          (height . 11)
          (top-edge . -14)
          (width . 12)
          (background . "bottons-bg")
          (foreground . "button-close")
          (class . close-button))
         ((right-edge . 0)
          (background . "bottons-bg")
          (foreground . "button-minimize")
          (height . 11)
          (width . 12)
          (top-edge . -14)
          (class . iconify-button))
         ((right-edge . 13)
          (font . "-schumacher-clean-medium-r-normal-*-*-120-*-*-c-*-iso646.1991-irv")
          (left-edge . 12)
          (height . 11)
          (top-edge . -14)
          (background . "caption")
          (y-justify . center)
          (x-justify . center)
          (text . window-name)
          (foreground . "#ffffffffffff")
          (class . title))
         ((height . 1)
          (background . "#000000000000")
          (top-edge . -1)
          (right-edge . -3)
          (left-edge . -4)))
        ("transient"
         ((height . 3)
          (top-edge . -17)
          (right-edge . -1)
          (left-edge . -1)
          (background . "top-border"))
         ((top-edge . -14)
          (height . 11)
          (left-edge . -1)
          (right-edge . -1)
          (background . "#cccccccccccc"))
         ((height . 3)
          (top-edge . -3)
          (left-edge . -1)
          (background . "top-bottom-border")
          (right-edge . -1))
         ((top-edge . -1)
          (bottom-edge . 12)
          (left-edge . -4)
          (background . "left-border"))
         ((height . 16)
          (top-edge . -17)
          (left-edge . -4)
          (background . "top-left-corner"))
         ((right-edge . -4)
          (background . "top-right-corner")
          (height . 16)
          (top-edge . -17))
         ((top-edge . -1)
          (background . "left-border")
          (bottom-edge . 12)
          (right-edge . -4))
         ((bottom-edge . -4)
          (background . "bottom-left-corner")
          (left-edge . -4))
         ((right-edge . -4)
          (bottom-edge . -4)
          (background . "dlg-bottom-right-corner"))
         ((right-edge . 12)
          (left-edge . 12)
          (background . "bottom-border")
          (bottom-edge . -4))
         ((font . "-schumacher-clean-medium-r-normal-*-*-120-*-*-c-*-iso646.1991-irv")
          (right-edge . 0)
          (left-edge . 12)
          (height . 11)
          (top-edge . -14)
          (background . "caption")
          (y-justify . center)
          (x-justify . center)
          (text . window-name)
          (foreground . "#ffffffffffff")
          (class . title))
         ((left-edge . -1)
          (foreground . "button-close")
          (background . "bottons-bg")
          (height . 11)
          (width . 12)
          (top-edge . -14)
          (class . close-button)))))

     (mapping-alist
      '((default . "default")
        (shaded . "shaded")
        (transient . "transient")
        (shaded-transient . "shaded")
        (shaped . "shaded")))

     (theme-name 'Studio))

  (add-frame-style
   theme-name (make-theme patterns-alist frames-alist mapping-alist))
  (when (boundp 'mark-frame-style-editable)
    (mark-frame-style-editable theme-name)))
