; 2YearsToNever: by Andrew Brehaut aka Gr1dl0ck
;
; This file is part of the 2YearsToNever Sawfish theme.
;
; 2YearsToNever is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
;
; 2YearsToNever is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this theme; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(let*
    ;; 14x17
    ((menu-images (list (make-image "in-menu.png")
			(make-image "an-menu.png") nil
                        (make-image "ap-menu.png")))
     (shaded-menu-images (list (make-image "in-sh-menu.png")
                               (make-image "an-sh-menu.png") nil
                               (make-image "ap-sh-menu.png")))


     ;; 12x17
     (minimize-images (list (make-image "in-minimize.png")
			    (make-image "an-minimize.png") nil
                            (make-image "ap-minimize.png")))

     ;; 11x17
     (jagged-top-images (list (make-image "i-jagged-top.png")
		              (make-image "a-jagged-top.png")))

     ;; 224x17
     (title-images (make-image "title.png"))

     ;; 4x17
     (top-right-corner-image (make-image "top-right-corner.png"))
     (shaded-top-right-corner-image (make-image "sh-top-right-corner.png"))

     ;; 4x4
     (bottom-left-corner-images (make-image "bottom-left-corner.png"))
     (bottom-right-corner-images (make-image "bottom-right-corner.png"))

     ;; 4x11
     (left-join-images (list (make-image "i-left-join.png")
			     (make-image "a-left-join.png")))

     ;; 4x38
     (left-border-images (list (make-image "i-left-border.png")
			      (make-image "a-left-border.png")))

     ;; 257x4
     (bottom-border-images (make-image "bottom-border.png"))

     ;; 4x49
     (right-border-images (make-image "right-border.png"))     

     ;; frame layout 

     (frame `(((background . ,menu-images)
              (top-edge . -17)
	       (left-edge . -4)
               (class . menu-button))

	      ((background . ,minimize-images)
               (top-edge . -17)
	       (left-edge . 10)
               (class . iconify-button))

              ((background . ,jagged-top-images)
               (top-edge . -17)
	       (left-edge . 22) 
	       (class . title))

	      ((background . ,title-images)
               (foreground . "black")
               (font . "-b&h-lucidux sans-medium-r-normal-*-*-80-*-*-p-*-iso8859-11")
               (text . ,window-name)
               (x-justify . 10)
               (y-justify . center)
	       (left-edge . 33)
	       (right-edge . 0)
	       (top-edge . -17)
	       (class . title))

              ((background . ,top-right-corner-image)
	       (right-edge . -4)
	       (top-edge . -17)
	       (class . top-right-corner))

	      ((background . ,left-border-images)
	       (left-edge . -4)
	       (top-edge . 0)
	       (bottom-edge . 11)
	       (class . left-border))

	      ((background . ,left-join-images)
	       (left-edge . -4)
	       (bottom-edge . 0)
	       (class . left-border))	
	
	      ((background . ,bottom-left-corner-images)
               (bottom-edge . -4)
               (left-edge . -4)
               (class . bottom-left-corner))
                              
	      ((background . ,bottom-border-images)
	       (left-edge . 0)
               (right-edge . 0)
               (bottom-edge . -4)
               (class . bottom-border))

	      ((background . ,bottom-right-corner-images)
               (right-edge . -4)
               (bottom-edge . -4)
               (class . bottom-right-corner))

	      ((background . ,right-border-images)
               (bottom-edge . 0)
	       (top-edge . 0)
	       (right-edge . -4)
               (class . right-border))))

     (shaped-frame `(((background . ,shaded-menu-images)
              (top-edge . -17)
	       (left-edge . -4)
               (class . menu-button))

	      ((background . ,minimize-images)
               (top-edge . -17)
	       (left-edge . 10)
               (class . iconify-button))

              ((background . ,jagged-top-images)
               (top-edge . -17)
	       (left-edge . 22) 
	       (class . title))

	      ((background . ,title-images)
               (foreground . "black")
               (font . "-b&h-lucidux sans-medium-r-normal-*-*-80-*-*-p-*-iso8859-11")
               (text . ,window-name)
               (x-justify . 10)
               (y-justify . center)
	       (left-edge . 33)
	       (right-edge . 0)
	       (top-edge . -17)
	       (class . title))

              ((background . ,shaded-top-right-corner-image)
	       (right-edge . -4)
	       (top-edge . -17)
	       (class . top-right-corner)))))

 (add-frame-style '2YearsToNever
                   (lambda (w type)
                     (case type
                       ((default) frame)
                       ((transient) frame)
                       ((shaped) shaped-frame)
                       ((shaped-transient) frame)))))
