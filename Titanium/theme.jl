;; Titanium/theme.jl

;; Copyright (C) 2001 Jes�s Gonz�lez <jgonzlz@terra.es>

;; Contributions for "Custom" button arrangement
;; by Michele Campeotto <micampe@f2s.com>

;; This theme is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; It is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with sawmill; see the file COPYING.  If not, write to
;; the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(defgroup
	Titanium
	"Titanium"
	:group appearance
)
  
(defcustom
	Titanium:ShowIcon t
	"Show window icon on titlebar"
	:user-level novice
	:type boolean
	:group (appearance Titanium)
)

(defcustom
	Titanium:ShowHandle t
	"Show resize handle"
	:user-level novice
	:type boolean
	:group (appearance Titanium)
)

(defcustom 
	Titanium:ButtonLayout 'Titanium
	"Arrangement of buttons on titlebar"
	:user-level novice
	:type symbol
	:options (Titanium Platinum Platinum+ Aqua Amiga FVWM2 KDE TWM WindowMaker Custom)
	:group (appearance Titanium)
)

(defcustom 
	Titanium:ButtonStyle 'Titanium
	"Style of buttons on titlebar"
	:user-level novice
	:type symbol
	:options (Titanium Platinum)
	:group (appearance Titanium)
)

(defcustom
	Titanium:DefaultIcon ""
	"Default Icon image"
	:user-level expert
	:type file-name
	:group (appearance Titanium)
)

(defcustom
	Titanium:TitleFont (get-font "-b&h-lucida-bold-r-normal-*-*-120-*-*-p-*-iso8859-1")
	"Titlebar font"
	:user-level expert
	:type font
	:group (appearance Titanium)
)
	
(defcustom
	Titanium:TransientTitleFont (get-font "-b&h-lucida-bold-r-normal-*-*-100-*-*-p-*-iso8859-1")
	"Transient titlebar font"
	:user-level expert
	:type font
	:group (appearance Titanium)
)
	
(defcustom
	Titanium:ActiveBaseColor (get-color "white")
	"Active decoration base color"
	:user-level expert
	:type color
	:group (appearance Titanium)
)

(defcustom
	Titanium:InactiveBaseColor (get-color "white")
	"Inactive decoration base color"
	:user-level expert
	:type color
	:group (appearance Titanium)
)

(defcustom
	Titanium:ActiveTitleColor (get-color "black")
	"Active title text color"
	:user-level expert
	:type color
	:group (appearance Titanium)
)

(defcustom
	Titanium:InactiveTitleColor (get-color "#6c6c6c")
	"Inactive title text color"
	:user-level expert
	:type color
	:group (appearance Titanium)
)
	
(defgroup
    LeftButtons
    "Custom left buttons"
    :group (appearance Titanium)
)
  
(defcustom 
    Titanium:LeftButton1 'Shade
    "First button on the left"
    :user-level novice
    :type symbol
    :options (Close Maximize Minimize Shade None)
    :group (appearance Titanium LeftButtons)
)

(defcustom 
    Titanium:LeftButton2 'Minimize
    "Second button on the left"
    :user-level novice
    :type symbol
    :options (Close Maximize Minimize Shade None)
    :group (appearance Titanium LeftButtons)
)

(defcustom 
    Titanium:LeftButton3 'Maximize
    "Third button on the left"
    :user-level novice
    :type symbol
    :options (Close Maximize Minimize Shade None)
    :group (appearance Titanium LeftButtons)
)

(defcustom 
    Titanium:LeftButton4 'None
    "Fourth button on the left"
    :user-level novice
    :type symbol
    :options (Close Maximize Minimize Shade None)
    :group (appearance Titanium LeftButtons)
)

(defgroup
    RightButtons
    "Custom right buttons"
    :group (appearance Titanium)
)
  
(defcustom 
    Titanium:RightButton1 'Close
    "First button on the right"
    :user-level novice
    :type symbol
    :options (Close Maximize Minimize Shade None)
    :group (appearance Titanium RightButtons)
)

(defcustom 
    Titanium:RightButton2 'None
    "Second button on the right"
    :user-level novice
    :type symbol
    :options (Close Maximize Minimize Shade None)
    :group (appearance Titanium RightButtons)
)

(defcustom 
    Titanium:RightButton3 'None
    "Third button on the right"
    :user-level novice
    :type symbol
    :options (Close Maximize Minimize Shade None)
    :group (appearance Titanium RightButtons)
)

(defcustom 
    Titanium:RightButton4 'None
    "Fourth button on the right"
    :user-level novice
    :type symbol
    :options (Close Maximize Minimize Shade None)
    :group (appearance Titanium RightButtons)
)

(letrec
	(
		dht dft dsw sdw gsw
		
		bt2 bt4 bt5 bt6 bt7 bt8 bt9 btA btB btC btD btE btF
		
		close-button-mask
		maximize-button-mask
		iconify-button-mask
		shade-button-mask
		transient-close-button-mask
		transient-maximize-button-mask
		transient-iconify-button-mask
		transient-shade-button-mask
	
		(default-icon (make-image "icon.png"))
		
		(user-icon ())
		
		(icon-image (lambda (win)
			(let
				((icon (window-icon-image win)))
				(if icon
					icon
					(if user-icon
						user-icon
						default-icon
					)
				)
			)
		))
		
		(make-image-set (lambda (width height)
			`(
				,(make-sized-image width height)
				,(make-sized-image width height)
			)
		))
		
		(make-button-set (lambda (width height)
			`(
				,(make-sized-image width height)
				,(make-sized-image width height)
				nil
				,(make-sized-image width height)
			)
		))
		
		;;
		;; Normal frames
		;;
		
		(button-default-mask [
			[bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 btC]
			[bt8 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 btF]
			[bt8 bt2 btF btC btC btC btC btC btC btC btC bt2 btF]
			[bt8 bt2 btC bt9 bt9 btA btA btB btB btC bt8 bt2 btF]
			[bt8 bt2 btC bt9 btA btA btB btB btC btC bt8 bt2 btF]
			[bt8 bt2 btC btA btA btB btB btC btC btD bt8 bt2 btF]
			[bt8 bt2 btC btA btB btB btC btC btD btD bt8 bt2 btF]
			[bt8 bt2 btC btB btB btC btC btD btD btE bt8 bt2 btF]
			[bt8 bt2 btC btB btC btC btD btD btE btE bt8 bt2 btF]
			[bt8 bt2 btC btC btC btD btD btE btE btF bt8 bt2 btF]
			[bt8 bt2 btC bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt2 btF]
			[bt8 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 btF]
			[btC btF btF btF btF btF btF btF btF btF btF btF btF]		
		])
		(button-clicked-mask [
			[bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 btC]
			[bt8 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 btF]
			[bt8 bt2 bt4 bt4 bt5 bt5 bt6 bt6 bt7 bt7 bt8 bt2 btF]
			[bt8 bt2 bt4 bt5 bt5 bt6 bt6 bt7 bt7 bt8 bt8 bt2 btF]
			[bt8 bt2 bt5 bt5 bt6 bt6 bt7 bt7 bt8 bt8 bt9 bt2 btF]
			[bt8 bt2 bt5 bt6 bt6 bt7 bt7 bt8 bt8 bt9 bt9 bt2 btF]
			[bt8 bt2 bt6 bt6 bt7 bt7 bt8 bt8 bt9 bt9 bt9 bt2 btF]
			[bt8 bt2 bt6 bt7 bt7 bt8 bt8 bt9 bt9 btA bt9 bt2 btF]
			[bt8 bt2 bt7 bt7 bt8 bt8 bt9 bt9 btA btA bt9 bt2 btF]
			[bt8 bt2 bt7 bt8 bt8 bt9 bt9 btA btA btB bt9 bt2 btF]
			[bt8 bt2 bt7 bt8 bt8 bt9 bt9 bt9 bt9 bt9 bt9 bt2 btF]
			[bt8 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 btF]
			[btC btF btF btF btF btF btF btF btF btF btF btF btF]
		])
		
		(close-image (make-button-set 13 13))
		(titanium-close-button-mask [
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil bt2 bt2 bt2 nil nil nil nil nil]
			[nil nil nil nil bt2 nil bt2 nil bt2 nil nil nil nil]
			[nil nil nil nil nil bt2 bt2 bt2 nil nil nil nil nil]
			[nil nil nil nil nil bt2 bt2 bt2 nil nil nil nil nil]
			[nil nil nil nil bt2 nil bt2 nil bt2 nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
		])
		(platinum-close-button-mask [
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
		])
		
		(maximize-image (make-button-set 13 13))
		(titanium-maximize-button-mask [
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil bt2 nil nil nil nil nil nil]
			[nil nil nil nil nil bt2 bt2 bt2 nil nil nil nil nil]
			[nil nil nil nil bt2 bt2 bt2 bt2 bt2 nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
		])
		(platinum-maximize-button-mask [
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil bt2 nil nil nil nil nil]
			[nil nil nil nil nil nil nil bt2 nil nil nil nil nil]
			[nil nil nil nil nil nil nil bt2 nil nil nil nil nil]
			[nil nil nil nil nil nil nil bt2 nil nil nil nil nil]
			[nil nil nil nil nil nil nil bt2 nil nil nil nil nil]
			[nil nil bt2 bt2 bt2 bt2 bt2 bt2 nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
		])
		
		(iconify-image (make-button-set 13 13))
		(titanium-iconify-button-mask [
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil bt2 bt2 bt2 bt2 bt2 nil nil nil nil]
			[nil nil nil nil nil bt2 bt2 bt2 nil nil nil nil nil]
			[nil nil nil nil nil nil bt2 nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
		])
		(platinum-iconify-button-mask [
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
		])
		
		(shade-image (make-button-set 13 13))
		(titanium-shade-button-mask [
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil bt2 bt2 bt2 nil nil nil nil nil]
			[nil nil nil nil nil bt2 bt2 bt2 nil nil nil nil nil]
			[nil nil nil nil nil bt2 bt2 bt2 nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
		])
		(platinum-shade-button-mask [
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil nil nil nil]
		])
		
		(title-image (make-image-set 1 1))
		(title-image-mask [
			[dft]
		])
		
		(side-top (make-image-set 1 4))
		(side-top-mask [
			[sdw]
			[dht]
			[dft]
			[dft]
		])

		(side-bottom (make-image-set 1 6))
		(side-bottom-mask [
			[sdw]
			[dht]
			[dft]
			[dft]
			[dsw]
			[sdw]
		])
		
		(side-subtitle (make-image-set 1 4))
		(side-subtitle-mask [
			[dft]
			[dft]
			[dsw]
			[sdw]
		])
		
		(sides (make-image-set 6 1))
		(sides-mask [
			[sdw dht dft dft dsw sdw]
		])

		(corner-top-side-left (make-image-set 6 1))
		(corner-top-side-left-mask [
			[sdw dht dft dft dft dft]
		])

		(corner-top-side-right (make-image-set 6 1))
		(corner-top-side-right-mask [
			[dft dft dft dft dsw sdw]
		])

		(junction-left (make-image-set 6 2))
		(junction-left-mask [
			[sdw dht dft dft dsw dsw]
			[sdw dht dft dft dsw sdw]
		])

		(junction-right (make-image-set 6 2))
		(junction-right-mask [
			[dsw dft dft dft dsw sdw]
			[sdw dht dft dft dsw sdw]
		])

		(corner-top-left (make-image-set 10 10))
		(corner-top-left-mask [
			[sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw]
			[sdw dht dht dht dht dht dht dht dht dht]
			[sdw dht dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft]
		])
		
		(corner-top-right (make-image-set 10 10))
		(corner-top-right-mask [
			[sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw]
			[dht dht dht dht dht dht dht dht dft sdw]			
			[dft dft dft dft dft dft dft dft dsw sdw]
			[dft dft dft dft dft dft dft dft dsw sdw]
			[dft dft dft dft dft dft dft dft dsw sdw]
			[dft dft dft dft dft dft dft dft dsw sdw]
			[dft dft dft dft dft dft dft dft dsw sdw]
			[dft dft dft dft dft dft dft dft dsw sdw]
			[dft dft dft dft dft dft dft dft dsw sdw]
			[dft dft dft dft dft dft dft dft dsw sdw]
		])

		(corner-bottom-left (make-image-set 10 10))
		(corner-bottom-left-mask [
			[sdw dht dft dft dsw sdw sdw sdw sdw sdw]
			[sdw dht dft dft dsw sdw sdw sdw sdw sdw]
			[sdw dht dft dft dsw sdw sdw sdw sdw sdw]
			[sdw dht dft dft dsw sdw sdw sdw sdw sdw]
			[sdw dht dft dft dsw sdw sdw sdw sdw sdw]
			[sdw dht dft dft dft dht dht dht dht dht]
			[sdw dht dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft]
			[sdw dft dsw dsw dsw dsw dsw dsw dsw dsw]
			[sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw]
		])
		
		(corner-bottom-right (make-image-set 10 10))
		(corner-bottom-right-mask [
			[sdw sdw sdw sdw sdw dht dft dft dsw sdw]
			[sdw sdw sdw sdw sdw dht dft dft dsw sdw]
			[sdw sdw sdw sdw sdw dht dft dft dsw sdw]
			[sdw sdw sdw sdw sdw dht dft dft dsw sdw]
			[sdw sdw sdw sdw sdw dht dft dft dsw sdw]
			[dht dht dht dht dht dht dft dft dsw sdw]
			[dft dft dft dft dft dft dft dft dsw sdw]
			[dft dft dft dft dft dft dft dft dsw sdw]
			[dsw dsw dsw dsw dsw dsw dsw dsw dsw sdw]
			[sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw]
		])
		
		(shadow (make-image-set 1 1))
		(shadow-mask [
			[sdw]
		])
		
		(grip (let
			((grp (make-image-set 4 4)))
			(image-put (last grp) 'tiled t)
			grp
		))
		(grip-mask [
			[gsw dft dft dft]
			[dft dht dft dft]
			[dft dft dft dft]
			[dft dft dft dft]
		])
				
		(handle (make-image-set 16 16))
		(handle-mask [
			[sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw sdw dht]
			[sdw dht dht dht dht dht dht dht dht dht dht dht dht dht dht dht]
			[sdw dht dft dft dft dft dft dft dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft gsw dft dft dft gsw dft dft dft gsw dft dft]
			[sdw dht dft dft dft dft dht dft dft dft dht dft dft dft dht dft]
			[sdw dht dft dft dft dft dft dft dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft gsw dft dft dft gsw dft dft dft gsw dft dft]
			[sdw dht dft dft dft dft dht dft dft dft dht dft dft dft dht dft]
			[sdw dht dft dft dft dft dft dft dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft dft dft dft dft dft dft dft dft dft dft dft]
			[sdw dht dft dft dft gsw dft dft dft gsw dft dft dft gsw dft dft]
			[sdw dht dft dft dft dft dht dft dft dft dht dft dft dft dht dft]
			[dht dht dft dft dft dft dft dft dft dft dft dft dft dft dft dft]
		])
		
		;;
		;; Transient frames
		;;

		(transient-button-default-mask [
			[bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 btC]
			[bt8 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 btF]
			[bt8 bt2 btF btC btC btC btC btC bt2 btF]
			[bt8 bt2 btC bt9 bt9 btA btC bt8 bt2 btF]
			[bt8 bt2 btC bt9 btA btC btD bt8 bt2 btF]
			[bt8 bt2 btC btA btC btD btE bt8 bt2 btF]
			[bt8 bt2 btC btC btD btE btF bt8 bt2 btF]
			[bt8 bt2 btC bt8 bt8 bt8 bt8 bt8 bt2 btF]
			[bt8 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 btF]
			[btC btF btF btF btF btF btF btF btF btF]		
		])
		(transient-button-clicked-mask [
			[bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 bt8 btC]
			[bt8 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 btF]
			[bt8 bt2 bt4 bt4 bt5 bt7 bt8 bt8 bt2 btF]
			[bt8 bt2 bt4 bt5 bt7 bt8 bt8 bt9 bt2 btF]
			[bt8 bt2 bt5 bt7 bt8 bt8 bt9 bt9 bt2 btF]
			[bt8 bt2 bt7 bt8 bt8 bt9 btA bt9 bt2 btF]
			[bt8 bt2 bt8 bt8 bt9 btA btB bt9 bt2 btF]
			[bt8 bt2 bt8 bt9 bt9 bt9 bt9 bt9 bt2 btF]
			[bt8 bt2 bt2 bt2 bt2 bt2 bt2 bt2 bt2 btF]
			[btC btF btF btF btF btF btF btF btF btF]
		])

		
		(transient-close-image (make-button-set 10 10))
		(titanium-transient-close-button-mask [
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil bt2 bt2 nil nil nil nil]
			[nil nil nil bt2 bt2 bt2 bt2 nil nil nil]
			[nil nil nil nil bt2 bt2 nil nil nil nil]
			[nil nil nil bt2 nil nil bt2 nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
		])
		(platinum-transient-close-button-mask [
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
		])
		
		(transient-maximize-image (make-button-set 10 10))
		(titanium-transient-maximize-button-mask [
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil bt2 bt2 nil nil nil nil]
			[nil nil nil bt2 bt2 bt2 bt2 nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
		])
		(platinum-transient-maximize-button-mask [
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil bt2 nil nil nil nil]
			[nil nil nil nil nil bt2 nil nil nil nil]
			[nil nil nil nil nil bt2 nil nil nil nil]
			[nil nil bt2 bt2 bt2 bt2 nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
		])
		
		(transient-iconify-image (make-button-set 10 10))
		(titanium-transient-iconify-button-mask [
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil bt2 bt2 bt2 bt2 nil nil nil]
			[nil nil nil nil bt2 bt2 nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
		])
		(platinum-transient-iconify-button-mask [
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil bt2 bt2 bt2 bt2 bt2 bt2 nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
		])
		
		(transient-shade-image (make-button-set 10 10))
		(titanium-transient-shade-button-mask [
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil bt2 bt2 nil nil nil nil]
			[nil nil nil nil bt2 bt2 nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
		])
		(platinum-transient-shade-button-mask [
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil bt2 bt2 bt2 bt2 bt2 bt2 nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil bt2 bt2 bt2 bt2 bt2 bt2 nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
			[nil nil nil nil nil nil nil nil nil nil]
		])

		(transient-side-top (make-image-set 1 2))
		(transient-side-top-mask [
			[sdw]
			[dht]
		])

		(transient-side-bottom (make-image-set 1 4))
		(transient-side-bottom-mask [
			[sdw]
			[dht]
			[dsw]
			[sdw]
		])
		
		(transient-side-subtitle (make-image-set 1 2))
		(transient-side-subtitle-mask [
			[dsw]
			[sdw]
		])
		
		(transient-sides (make-image-set 4 1))
		(transient-sides-mask [
			[sdw dht dsw sdw]
		])

		(transient-corner-top-side-left (make-image-set 2 1))
		(transient-corner-top-side-left-mask [
			[sdw dht]
		])

		(transient-corner-top-side-right (make-image-set 2 1))
		(transient-corner-top-side-right-mask [
			[dsw sdw]
		])

		(transient-junction-left (make-image-set 4 2))
		(transient-junction-left-mask [
			[sdw dht dsw dsw]
			[sdw dht dsw sdw]
		])

		(transient-junction-right (make-image-set 4 2))
		(transient-junction-right-mask [
			[dsw dft dsw sdw]
			[sdw dht dsw sdw]
		])

		(transient-corner-top-left (make-image-set 5 5))
		(transient-corner-top-left-mask [
			[sdw sdw sdw sdw sdw]
			[sdw dht dht dht dht]
			[sdw dht dft dft dft]
			[sdw dht dft dft dft]
			[sdw dht dft dft dft]
		])
		
		(transient-corner-top-right (make-image-set 5 5))
		(transient-corner-top-right-mask [
			[sdw sdw sdw sdw sdw]
			[dht dht dht dft sdw]			
			[dft dft dft dsw sdw]
			[dft dft dft dsw sdw]
			[dft dft dft dsw sdw]
		])

		(transient-corner-bottom-left (make-image-set 5 5))
		(transient-corner-bottom-left-mask [
			[sdw dht dsw sdw sdw]
			[sdw dht dsw sdw sdw]
			[sdw dht dft dht dht]
			[sdw dft dsw dsw dsw]
			[sdw sdw sdw sdw sdw]
		])
		
		(transient-corner-bottom-right (make-image-set 5 5))
		(transient-corner-bottom-right-mask [
			[sdw sdw dht dsw sdw]
			[sdw sdw dht dsw sdw]
			[dht dht dht dsw sdw]
			[dsw dsw dsw dsw sdw]
			[sdw sdw sdw sdw sdw]
		])
		
		(transient-grip (let
			((grp (make-image-set 3 3)))
			(image-put (last grp) 'tiled t)
			grp
		))
		(transient-grip-mask [
			[gsw dft dft]
			[dft dht dft]
			[dft dft dft]
		])
		
		;;
		;;
		;;
				
		left-button-list right-button-list
		
		(build-theme (lambda ()
			(letrec
				(
					(image-generator (lambda (mask)
						(lambda (x y)
							(color-rgb-8 (if (consp mask)
								(or
									(symbol-value (aref (aref (cdr mask) y) x))
									(symbol-value (aref (aref (car mask) y) x))
								)
								(symbol-value (aref (aref mask y) x))
							))
						)
					))
					
					(generate-image (lambda (index)
						(lambda (image-mask)
							(image-fill
								(image-generator (cdr image-mask))
								(nth index (car image-mask))
							)
						)
					))
					
					(generate-color (lambda (index base)
						(let*
							(
								(syn-color (mapcar
									(lambda (color)
										(/ (* color index) 255)
									)
									(color-rgb base)
								))
							)
							(get-color-rgb
								(nth 0 syn-color)
								(nth 1 syn-color)
								(nth 2 syn-color)
							)
						)
					))
					
					(create-button-list (lambda (button-list)
						(mapcar
							(lambda (button-type)
								(case button-type
									((Close) 'close-button)
									((Minimize) 'iconify-button)
									((Maximize) 'maximize-button)
									((Shade) 'shade-button)
							))
							(delete 'None button-list)
						)
					))
                    
					(get-custom-button-list (lambda (side)
						(case side
							((left)
								(create-button-list `(
									,Titanium:LeftButton1
									,Titanium:LeftButton2
									,Titanium:LeftButton3
									,Titanium:LeftButton4
								))
							)
							((right)
								(create-button-list `(
									,Titanium:RightButton1
									,Titanium:RightButton2
									,Titanium:RightButton3
									,Titanium:RightButton4
								))
					  		)
						)
					))
					
					(mapset (lambda (symbol-list value-list)
						(when (and symbol-list value-list)
							(set (car symbol-list) (car value-list))
							(mapset (cdr symbol-list) (cdr value-list))
						)
					))
				)
				
				;; set button layout
				
				(setq left-button-list
					(case Titanium:ButtonLayout
						((Platinum Platinum+ Amiga FVWM2 TWM) '(close-button))
						((Aqua) '(close-button iconify-button maximize-button))
						((KDE) '(shade-button))
						((Titanium) '())
						((WindowMaker) '(iconify-button))
						((Custom) (get-custom-button-list 'left))
					)
				)
				
				(setq right-button-list
					(case Titanium:ButtonLayout
						((Platinum) '(shade-button maximize-button))
						((Platinum+) '(iconify-button maximize-button))
						((Amiga) '(maximize-button iconify-button))
						((FVWM2) '(maximize-button iconify-button shade-button))
						((TWM) '())
						((Aqua) '(shade-button))
						((KDE Titanium) '(close-button maximize-button iconify-button))
						((WindowMaker) '(close-button))
						((Custom) (get-custom-button-list 'right))
					)
				)
				
				;; set button style
				
				(mapset
					'(
						close-button-mask
						maximize-button-mask
						iconify-button-mask
						shade-button-mask
						transient-close-button-mask
						transient-maximize-button-mask
						transient-iconify-button-mask
						transient-shade-button-mask
					)
					(case Titanium:ButtonStyle
						((Titanium) `(
							,titanium-close-button-mask
							,titanium-maximize-button-mask
							,titanium-iconify-button-mask
							,titanium-shade-button-mask
							,titanium-transient-close-button-mask
							,titanium-transient-maximize-button-mask
							,titanium-transient-iconify-button-mask
							,titanium-transient-shade-button-mask
						))
						((Platinum) `(
							,platinum-close-button-mask
							,platinum-maximize-button-mask
							,platinum-iconify-button-mask
							,platinum-shade-button-mask
							,platinum-transient-close-button-mask
							,platinum-transient-maximize-button-mask
							,platinum-transient-iconify-button-mask
							,platinum-transient-shade-button-mask
						))
					)
				)
				
				;; load default icon
				
				(if (eql 0 (length Titanium:DefaultIcon))
					(setq user-icon ())
					(setq user-icon (make-image Titanium:DefaultIcon))
				)
				
				;; set inactive colors
				
				(setq dht (generate-color #xdd Titanium:InactiveBaseColor))
				(setq dft (generate-color #xdd Titanium:InactiveBaseColor))
				(setq dsw (generate-color #xdd Titanium:InactiveBaseColor))
				(setq gsw (generate-color #xdd Titanium:InactiveBaseColor))
				(setq sdw (generate-color #x55 Titanium:InactiveBaseColor))
				
				(setq bt2 (generate-color #xdd Titanium:InactiveBaseColor))
				(setq bt4 (generate-color #xdd Titanium:InactiveBaseColor))
				(setq bt5 (generate-color #xdd Titanium:InactiveBaseColor))
				(setq bt6 (generate-color #xdd Titanium:InactiveBaseColor))
				(setq bt7 (generate-color #xdd Titanium:InactiveBaseColor))
				(setq bt8 (generate-color #xdd Titanium:InactiveBaseColor))
				(setq bt9 (generate-color #xdd Titanium:InactiveBaseColor))
				(setq btA (generate-color #xdd Titanium:InactiveBaseColor))
				(setq btB (generate-color #xdd Titanium:InactiveBaseColor))
				(setq btC (generate-color #xdd Titanium:InactiveBaseColor))
				(setq btD (generate-color #xdd Titanium:InactiveBaseColor))
				(setq btE (generate-color #xdd Titanium:InactiveBaseColor))
				(setq btF (generate-color #xdd Titanium:InactiveBaseColor))
				
				;; generate inactive decorations
				
				(mapc
					(generate-image 0)
					`(
						;; Common sets
						(,title-image . ,title-image-mask)
						(,shadow . ,shadow-mask)
						
						;; Default sets
						(,corner-top-left . ,corner-top-left-mask)
						(,corner-top-right . ,corner-top-right-mask)
						(,corner-bottom-left . ,corner-bottom-left-mask)
						(,corner-bottom-right . ,corner-bottom-right-mask)
						(,side-top . ,side-top-mask)
						(,side-bottom . ,side-bottom-mask)
						(,sides . ,sides-mask)
						(,side-subtitle . ,side-subtitle-mask)
						(,junction-left . ,junction-left-mask)
						(,junction-right . ,junction-right-mask)
						(,corner-top-side-left . ,corner-top-side-left-mask)
						(,corner-top-side-right . ,corner-top-side-right-mask)
						(,grip . ,grip-mask)
						(,handle . ,handle-mask)
						
						;; Transient sets
						(,transient-corner-top-left . ,transient-corner-top-left-mask)
						(,transient-corner-top-right . ,transient-corner-top-right-mask)
						(,transient-corner-bottom-left . ,transient-corner-bottom-left-mask)
						(,transient-corner-bottom-right . ,transient-corner-bottom-right-mask)
						(,transient-side-top . ,transient-side-top-mask)
						(,transient-side-bottom . ,transient-side-bottom-mask)
						(,transient-sides . ,transient-sides-mask)
						(,transient-side-subtitle . ,transient-side-subtitle-mask)
						(,transient-junction-left . ,transient-junction-left-mask)
						(,transient-junction-right . ,transient-junction-right-mask)
						(,transient-corner-top-side-left . ,transient-corner-top-side-left-mask)
						(,transient-corner-top-side-right . ,transient-corner-top-side-right-mask)
						(,transient-grip . ,transient-grip-mask)
						
						;; Buttons
						(
							,close-image
							.
							(,button-default-mask . ,close-button-mask)
						)
						(
							,maximize-image
							.
							(,button-default-mask . ,maximize-button-mask)
						)
						(
							,iconify-image
							.
							(,button-default-mask . ,iconify-button-mask)
						)
						(
							,shade-image
							.
							(,button-default-mask . ,shade-button-mask)
						)
						(
							,transient-close-image
							. 
							(,transient-button-default-mask . ,transient-close-button-mask)
						)
						(
							,transient-maximize-image
							.
							(,transient-button-default-mask . ,transient-maximize-button-mask)
						)
						(
							,transient-iconify-image
							.
							(,transient-button-default-mask . ,transient-iconify-button-mask)
						)
						(
							,transient-shade-image
							.
							(,transient-button-default-mask . ,transient-shade-button-mask)
						)
					)
				)
				
				;; set active colors
				
				(setq dht (generate-color #xff Titanium:ActiveBaseColor))
				(setq dft (generate-color #xcc Titanium:ActiveBaseColor))
				(setq dsw (generate-color #x99 Titanium:ActiveBaseColor))
				(setq gsw (generate-color #x77 Titanium:ActiveBaseColor))
				(setq sdw (generate-color #x00 Titanium:ActiveBaseColor))
				
				(setq bt2 (generate-color #x22 Titanium:ActiveBaseColor))
				(setq bt4 (generate-color #x44 Titanium:ActiveBaseColor))
				(setq bt5 (generate-color #x55 Titanium:ActiveBaseColor))
				(setq bt6 (generate-color #x66 Titanium:ActiveBaseColor))
				(setq bt7 (generate-color #x77 Titanium:ActiveBaseColor))
				(setq bt8 (generate-color #x88 Titanium:ActiveBaseColor))
				(setq bt9 (generate-color #x99 Titanium:ActiveBaseColor))
				(setq btA (generate-color #xaa Titanium:ActiveBaseColor))
				(setq btB (generate-color #xbb Titanium:ActiveBaseColor))
				(setq btC (generate-color #xcc Titanium:ActiveBaseColor))
				(setq btD (generate-color #xdd Titanium:ActiveBaseColor))
				(setq btE (generate-color #xee Titanium:ActiveBaseColor))
				(setq btF (generate-color #xff Titanium:ActiveBaseColor))
				
				;; generate active decorations
				
				(mapc
					(generate-image 1)
					`(
						;; Common sets
						(,title-image . ,title-image-mask)
						(,shadow . ,shadow-mask)
						
						;; Default sets
						(,corner-top-left . ,corner-top-left-mask)
						(,corner-top-right . ,corner-top-right-mask)
						(,corner-bottom-left . ,corner-bottom-left-mask)
						(,corner-bottom-right . ,corner-bottom-right-mask)
						(,side-top . ,side-top-mask)
						(,side-bottom . ,side-bottom-mask)
						(,sides . ,sides-mask)
						(,side-subtitle . ,side-subtitle-mask)
						(,junction-left . ,junction-left-mask)
						(,junction-right . ,junction-right-mask)
						(,corner-top-side-left . ,corner-top-side-left-mask)
						(,corner-top-side-right . ,corner-top-side-right-mask)
						(,grip . ,grip-mask)
						(,handle . ,handle-mask)
						
						;; Transient sets
						(,transient-corner-top-left . ,transient-corner-top-left-mask)
						(,transient-corner-top-right . ,transient-corner-top-right-mask)
						(,transient-corner-bottom-left . ,transient-corner-bottom-left-mask)
						(,transient-corner-bottom-right . ,transient-corner-bottom-right-mask)
						(,transient-side-top . ,transient-side-top-mask)
						(,transient-side-bottom . ,transient-side-bottom-mask)
						(,transient-sides . ,transient-sides-mask)
						(,transient-side-subtitle . ,transient-side-subtitle-mask)
						(,transient-junction-left . ,transient-junction-left-mask)
						(,transient-junction-right . ,transient-junction-right-mask)
						(,transient-corner-top-side-left . ,transient-corner-top-side-left-mask)
						(,transient-corner-top-side-right . ,transient-corner-top-side-right-mask)
						(,transient-grip . ,transient-grip-mask)
						
						;; Buttons
						(
							,close-image
							.
							(,button-default-mask . ,close-button-mask)
						)
						(
							,maximize-image
							.
							(,button-default-mask . ,maximize-button-mask)
						)
						(
							,iconify-image
							.
							(,button-default-mask . ,iconify-button-mask)
						)
						(
							,shade-image
							.
							(,button-default-mask . ,shade-button-mask)
						)
						(
							,transient-close-image
							. 
							(,transient-button-default-mask . ,transient-close-button-mask)
						)
						(
							,transient-maximize-image
							.
							(,transient-button-default-mask . ,transient-maximize-button-mask)
						)
						(
							,transient-iconify-image
							.
							(,transient-button-default-mask . ,transient-iconify-button-mask)
						)
						(
							,transient-shade-image
							.
							(,transient-button-default-mask . ,transient-shade-button-mask)
						)
					)
				)
				
				;; generate clicked buttons
				
				(mapc
					(generate-image 3)
					`(
						;; Buttons
						(
							,close-image
							.
							(,button-clicked-mask . ,close-button-mask)
						)
						(
							,maximize-image
							.
							(,button-clicked-mask . ,maximize-button-mask)
						)
						(
							,iconify-image
							.
							(,button-clicked-mask . ,iconify-button-mask)
						)
						(
							,shade-image
							.
							(,button-clicked-mask . ,shade-button-mask)
						)
						(
							,transient-close-image
							. 
							(,transient-button-clicked-mask . ,transient-close-button-mask)
						)
						(
							,transient-maximize-image
							.
							(,transient-button-clicked-mask . ,transient-maximize-button-mask)
						)
						(
							,transient-iconify-image
							.
							(,transient-button-clicked-mask . ,transient-iconify-button-mask)
						)
						(
							,transient-shade-image
							.
							(,transient-button-clicked-mask . ,transient-shade-button-mask)
						)
					)
				)
			)
		))
		
		(title-area-width (lambda (win type)
			(let
				((lst (copy-sequence (append right-button-list left-button-list))))
				(mapc
					(lambda (class)
						(delete class lst)
					)
					(window-get win 'removed-classes)
				)
				(max 0 (-
					(+ (car (window-dimensions win)) 4)
					(*
						(case type ((default) 16) ((transient) 11))
						(length lst)
					)
				))
			)
		))
		
		(title-offset (lambda (win type)
			(let
				((lst (copy-sequence left-button-list)))
				(mapc
					(lambda (class)
						(delete class lst)
					)
					(window-get win 'removed-classes)
				)
				(-
					(*
						(case type ((default) 16) ((transient) 11))
						(length lst)
					)
					2
				)
			)
		))
		
		(title-font (lambda ()
			Titanium:TitleFont
		))
		
		(transient-title-font (lambda ()
			Titanium:TransientTitleFont
		))
		
		(title-width (lambda (type) (lambda (win)
			(min 
				(max 0 (-
					(title-area-width win type)
					(case type ((default) 24) ((transient) 15))
				))
				(text-width
					(window-name win)
					(case type
						((default) (title-font))
						((transient) (transient-title-font))
					)
				)
			)
		)))
		
		(icon-justify (lambda (type) (lambda (win)
			(+
				(/ 
					(-
						(title-area-width win type)
						(+
							((title-width type) win)
							(if Titanium:ShowIcon
								(case type ((default) 24) ((transient) 15))
								0
							)
						)
					)
					2
				)
				(title-offset win type)
			)
		)))
	  
		(title-justify (lambda (type) (lambda (win)
			(+
				((icon-justify type) win)
				(if Titanium:ShowIcon
					(case type ((default) 24) ((transient) 15))
					0
				)
			)
		)))
	  
		(hide-icon (lambda (win)
			(not Titanium:ShowIcon)
		))
		
		(hide-handle (lambda (win)
			(not Titanium:ShowHandle)
		))
		
		(title-text-color (lambda (win)
			`(,Titanium:InactiveTitleColor ,Titanium:ActiveTitleColor)
		))
		
		(title-parts `(
			(
				(class . title)
				(background . ,title-image)
				(left-edge . -2)
				(right-edge . -2)
				(top-edge . -18)
				(height . 14)
				(cursor . left_ptr)
			)
			(
				(class . title)
				(background . ,title-image)
				(foreground . ,title-text-color)
				(font . ,title-font)
				(text . ,window-name)
				(x-justify . center)
				(y-justify . center)
				(left-edge . ,(title-justify 'default))
				(width . ,(title-width 'default))
				(top-edge . -18)
				(height . 14)
				(cursor . left_ptr)
			)
			(
				(class . title)
				(background . ,icon-image)
				(top-edge . -21)
				(left-edge . ,(icon-justify 'default))
				(width . 18)
				(height . 18)
				(hidden . ,hide-icon)
				(cursor . left_ptr)
			)
		))
				
		(transient-title-parts `(
			(
				(class . title)
				(background . ,title-image)
				(left-edge . -2)
				(right-edge . -2)
				(top-edge . -14)
				(height . 12)
				(cursor . left_ptr)
			)
			(
				(class . title)
				(background . ,title-image)
				(foreground . ,title-text-color)
				(font . ,transient-title-font)
				(text . ,window-name)
				(x-justify . center)
				(y-justify . center)
				(left-edge . ,(title-justify 'transient))
				(width . ,(title-width 'transient))
				(top-edge . -14)
				(height . 12)
				(cursor . left_ptr)
			)
			(
				(class . title)
				(background . ,icon-image)
				(top-edge . -14)
				(left-edge . ,(icon-justify 'transient))
				(width . 12)
				(height . 12)
				(hidden . ,hide-icon)
				(cursor . left_ptr)
			)
		))
				
		(border-parts `(
			(
				(class . left-border)
				(left-edge . -6)
				(top-edge . -2)
				(background . ,junction-left)
			)
			(
				(class . left-border)
				(left-edge . -6)
				(top-edge . -12)
				(height . 10)
				(background . ,corner-top-side-left)
			)
			(
				(class . top-left-corner)
				(background . ,corner-top-left)
				(left-edge . -6)
				(top-edge . -22)
			)
			(
				(class . top-border)
				(background . ,side-top)
				(left-edge . 4)
				(right-edge . 4)
				(top-edge . -22)
			)
			(
				(class . top-right-corner)
				(background . ,corner-top-right)
				(right-edge . -6)
				(top-edge . -22)
			)
			(
				(class . right-border)
				(right-edge . -6)
				(top-edge . -12)
				(height . 10)
				(background . ,corner-top-side-right)
			)
			(
				(class . right-border)
				(right-edge . -6)
				(top-edge . -2)
				(background . ,junction-right)
			)
			(
				(background . ,side-subtitle)
				(left-edge  . 0)
				(right-edge . 0)
				(top-edge   . -4)
			)
			(
				(class . right-border)
				(top-edge . 0)
				(bottom-edge . 4)
				(background . ,sides)
				(right-edge . -6)
				(below-client . t)
			)
			(
				(class . bottom-right-corner)
				(background . ,corner-bottom-right)
				(right-edge . -6)
				(bottom-edge . -6)
				(below-client . t)
			)
			(
				(class . bottom-right-corner)
				(background . ,handle)
				(right-edge . -2)
				(bottom-edge . -2)
				(hidden . ,hide-handle)
			)
			(
				(class . bottom-border)
				(background . ,side-bottom)
				(left-edge . 4)
				(right-edge . 4)
				(bottom-edge . -6)
				(below-client . t)
			)
			(
				(class . bottom-left-corner)
				(background . ,corner-bottom-left)
				(left-edge . -6)
				(bottom-edge . -6)
				(below-client . t)
			)
			(
				(class . left-border)
				(background . ,sides)
				(left-edge . -6)
				(top-edge . 0)
				(bottom-edge . 4)
				(below-client . t)
			)
			;; Shadows
			(
				(background . ,shadow)
				(left-edge . -4)
				(right-edge . -7)
				(bottom-edge . -7)
				(height . 1)
			)
			(
				(background . ,shadow)
				(right-edge . -7)
				(width . 1)
				(top-edge . -20)
				(bottom-edge . -7)
			)
		))
		
		(transient-border-parts `(
			(
				(class . left-border)
				(left-edge . -4)
				(top-edge . -2)
				(background . ,transient-junction-left)
			)
			(
				(class . left-border)
				(left-edge . -4)
				(top-edge . -11)
				(height . 9)
				(background . ,transient-corner-top-side-left)
			)
			(
				(class . top-left-corner)
				(background . ,transient-corner-top-left)
				(left-edge . -4)
				(top-edge . -16)
			)
			(
				(class . top-border)
				(background . ,transient-side-top)
				(left-edge . 1)
				(right-edge . 1)
				(top-edge . -16)
			)
			(
				(class . top-right-corner)
				(background . ,transient-corner-top-right)
				(right-edge . -4)
				(top-edge . -16)
			)
			(
				(class . right-border)
				(right-edge . -4)
				(top-edge . -11)
				(height . 9)
				(background . ,transient-corner-top-side-right)
			)
			(
				(class . right-border)
				(right-edge . -4)
				(top-edge . -2)
				(background . ,transient-junction-right)
			)
			(
				(background . ,transient-side-subtitle)
				(left-edge  . 0)
				(right-edge . 0)
				(top-edge   . -2)
			)
			(
				(class . right-border)
				(top-edge . 0)
				(bottom-edge . 1)
				(background . ,transient-sides)
				(right-edge . -4)
				(below-client . t)
			)
			(
				(class . bottom-right-corner)
				(background . ,transient-corner-bottom-right)
				(right-edge . -4)
				(bottom-edge . -4)
				(below-client . t)
			)
			(
				(class . bottom-border)
				(background . ,transient-side-bottom)
				(left-edge . 1)
				(right-edge . 1)
				(bottom-edge . -4)
				(below-client . t)
			)
			(
				(class . bottom-left-corner)
				(background . ,transient-corner-bottom-left)
				(left-edge . -4)
				(bottom-edge . -4)
				(below-client . t)
			)
			(
				(class . left-border)
				(background . ,transient-sides)
				(left-edge . -4)
				(top-edge . 0)
				(bottom-edge . 1)
				(below-client . t)
			)
			;; Shadows
			(
				(background . ,shadow)
				(left-edge . -2)
				(right-edge . -5)
				(bottom-edge . -5)
				(height . 1)
			)
			(
				(background . ,shadow)
				(right-edge . -5)
				(width . 1)
				(top-edge . -14)
				(bottom-edge . -5)
			)
		))
		
		(shaped-parts `(
			(
				(class . title)
				(background . ,corner-top-left)
				(left-edge . -6)
				(top-edge . -22)
			)
			(
				(class . title)
				(background . ,side-top)
				(left-edge . 4)
				(right-edge . 4)
				(top-edge . -22)
			)
			(
				(class . title)
				(background . ,corner-top-right)
				(right-edge . -6)
				(top-edge . -22)
			)
			(
				(class . title)
				(top-edge . -12)
				(height . 2)
				(background . ,corner-top-side-right)
				(right-edge . -6)
			)
			(
				(class . title)
				(background . ,corner-bottom-right)
				(right-edge . -6)
				(top-edge . -10)
			)
			(
				(class . title)
				(background . ,side-subtitle)
				(left-edge . 4)
				(right-edge . 4)
				(top-edge . -4)
			)
			(
				(class . title)
				(background . ,corner-bottom-left)
				(left-edge . -6)
				(top-edge . -10)
			)
			(
				(class . title)
				(top-edge . -12)
				(height . 2)
				(background . ,corner-top-side-left)
				(left-edge . -6)
			)
			;; Shadows
			(
				(background . ,shadow)
				(left-edge . -4)
				(right-edge . -7)
				(top-edge . 0)
				(height . 1)
			)
			(
				(background . ,shadow)
				(right-edge . -7)
				(width . 1)
				(top-edge . -20)
				(height . 20)
			)
		))

		(transient-shaped-parts `(
			(
				(class . title)
				(background . ,transient-corner-top-left)
				(left-edge . -4)
				(top-edge . -16)
			)
			(
				(class . title)
				(background . ,transient-side-top)
				(left-edge . 1)
				(right-edge . 1)
				(top-edge . -16)
			)
			(
				(class . title)
				(background . ,transient-corner-top-right)
				(right-edge . -4)
				(top-edge . -16)
			)
			(
				(class . title)
				(top-edge . -11)
				(height . 6)
				(background . ,transient-corner-top-side-right)
				(right-edge . -4)
			)
			(
				(class . title)
				(background . ,transient-corner-bottom-right)
				(right-edge . -4)
				(top-edge . -5)
			)
			(
				(class . title)
				(background . ,transient-side-subtitle)
				(left-edge . 1)
				(right-edge . 1)
				(top-edge . -2)
			)
			(
				(class . title)
				(background . ,transient-corner-bottom-left)
				(left-edge . -4)
				(top-edge . -5)
			)
			(
				(class . title)
				(top-edge . -11)
				(height . 6)
				(background . ,transient-corner-top-side-left)
				(left-edge . -4)
			)
			;; Shadows
			(
				(background . ,shadow)
				(left-edge . -2)
				(right-edge . -5)
				(top-edge . 0)
				(height . 1)
			)
			(
				(background . ,shadow)
				(right-edge . -5)
				(width . 1)
				(top-edge . -14)
				(height . 14)
			)
		))

		(grip-width (lambda (pos edge type) (lambda (win)
			(case edge
				((left-edge)
					(- ((icon-justify type) win) pos 12)
				)
				((right-edge)
					(-
						(car (window-dimensions win))
						((icon-justify type) win)
						(if Titanium:ShowIcon
							(case type ((default) 38) ((transient) 29))
							14
						)
						((title-width type) win)
						pos
					)
				)
			)
		)))
		
		(place-grip (lambda (pos edge type)
			(case type
				((default)
					`(
						(class . title)
						(background . ,grip)
						(,edge . ,(+ pos 5))
						(width . ,(grip-width pos edge type))
						(top-edge . -16)
						(height . 12)
						(cursor . left_ptr)
					)
				)
				((transient)
					`(
						(class . title)
						(background . ,transient-grip)
						(,edge . ,(+ pos 5))
						(width . ,(grip-width pos edge type))
						(top-edge . -12)
						(height . 9)
						(cursor . left_ptr)
					)
				)
			)
		))
		
		(place-close-button (lambda (pos edge type)
			(case type
				((default)
					`(
						(class . close-button)
						(background . ,close-image)
						(,edge . ,pos)
						(top-edge . -18)
					)
				)
				((transient)
					`(
						(class . close-button)
						(background . ,transient-close-image)
						(,edge . ,pos)
						(top-edge . -13)
					)
				)
			)
		))
		
		(place-shade-button (lambda (pos edge type)
			(case type
				((default)
					`(
						(class . shade-button)
						(background . ,shade-image)
						(,edge . ,pos)
						(top-edge . -18)
					)
				)
				((transient)
					`(
						(class . shade-button)
						(background . ,transient-shade-image)
						(,edge . ,pos)
						(top-edge . -13)
					)
				)
			)
		))
		
		(place-maximize-button (lambda (pos edge type)
			(case type
				((default)
					`(
						(class . maximize-button)
						(background . ,maximize-image)
						(,edge . ,pos)
						(top-edge . -18)
					)
				)
				((transient)
					`(
						(class . maximize-button)
						(background . ,transient-maximize-image)
						(,edge . ,pos)
						(top-edge . -13)
					)
				)
			)
		))
		
		(place-iconify-button (lambda (pos edge type)
			(case type
				((default)
					`(
						(class . iconify-button)
						(background . ,iconify-image)
						(,edge . ,pos)
						(top-edge . -18)
					)
				)
				((transient)
					`(
						(class . iconify-button)
						(background . ,transient-iconify-image)
						(,edge . ,pos)
						(top-edge . -13)
					)
				)
			)
		))
		
		(place-button (lambda (button-type pos edge type)
			(case button-type
				((close-button) (place-close-button pos edge type))
				((maximize-button) (place-maximize-button pos edge type))
				((iconify-button) (place-iconify-button pos edge type))
				((shade-button) (place-shade-button pos edge type))
				((grip) (place-grip pos edge type))
			)
		))
				
		(place-buttons (lambda (button-list pos edge frame-type)
			(when button-list `(
				,(place-button (car button-list) pos edge frame-type)
				,@(place-buttons 
					(cdr button-list)
					(+
						pos 
						(case frame-type
							((default) 16)
							((transient) 11)
						)
					)
					edge
					frame-type
				)
			))
		))
              
		(button-parts (lambda (win frame-type)
			(let
				(
					(lbl (copy-sequence left-button-list))
					(rbl (copy-sequence right-button-list))
				)
				(mapc
					(lambda (class)
						(delete class lbl)
						(delete class rbl)
					)
					(window-get win 'removed-classes)
				)
				`(
					,@(place-buttons (append lbl '(grip)) -2 'left-edge frame-type)
					,@(place-buttons (append rbl '(grip)) -2 'right-edge frame-type)
				)
			)
		))

		(default-frame (lambda (win)
			`(,@border-parts ,@title-parts ,@(button-parts win 'default))
		))
		
		(transient-frame (lambda (win)
			`(,@transient-border-parts ,@transient-title-parts ,@(button-parts win 'transient))
		))
		
		(shaped-frame (lambda (win)
			`(,@shaped-parts ,@title-parts ,@(button-parts win 'default))
		))
		
		(shaped-transient-frame (lambda (win)
			`(,@transient-shaped-parts ,@transient-title-parts ,@(button-parts win 'transient))
		))
		
		(rebuild-theme (lambda ()
			(build-theme)
			(reframe-windows-with-style 'Titanium)
		))
		      
	)

	(build-theme)
	
	(call-after-property-changed
		'(WM_NAME WM_HINTS)
		(lambda (win) (rebuild-frame win))
	)
	
	(def-frame-class
		shade-button
		nil
		(bind-keys shade-button-keymap "Button1-Off" 'toggle-window-shaded)
	)

	(add-frame-style 'Titanium
		(lambda (win type)
			(case type
				((default) (default-frame win))
				((transient) (transient-frame win))
				((shaped) (shaped-frame win))
				((shaped-transient) (shaped-transient-frame win))
			)
		)
        
	)
	
	(custom-set-property 'Titanium:ShowIcon ':after-set rebuild-theme)
	(custom-set-property 'Titanium:ShowHandle ':after-set rebuild-theme)
	(custom-set-property 'Titanium:ButtonLayout ':after-set rebuild-theme)
	(custom-set-property 'Titanium:ButtonStyle ':after-set rebuild-theme)
	(custom-set-property 'Titanium:TitleFont ':after-set rebuild-theme)
	(custom-set-property 'Titanium:TransientTitleFont ':after-set rebuild-theme)
	(custom-set-property 'Titanium:ActiveBaseColor ':after-set rebuild-theme)
	(custom-set-property 'Titanium:InactiveBaseColor ':after-set rebuild-theme)
	(custom-set-property 'Titanium:ActiveTitleColor ':after-set rebuild-theme)
	(custom-set-property 'Titanium:InactiveTitleColor ':after-set rebuild-theme)
	(custom-set-property 'Titanium:DefaultIcon ':after-set rebuild-theme)
	(custom-set-property 'Titanium:LeftButton1 ':after-set rebuild-theme)
	(custom-set-property 'Titanium:LeftButton2 ':after-set rebuild-theme)
	(custom-set-property 'Titanium:LeftButton3 ':after-set rebuild-theme)
	(custom-set-property 'Titanium:LeftButton4 ':after-set rebuild-theme)
	(custom-set-property 'Titanium:RightButton1 ':after-set rebuild-theme)
	(custom-set-property 'Titanium:RightButton2 ':after-set rebuild-theme)
	(custom-set-property 'Titanium:RightButton3 ':after-set rebuild-theme)
	(custom-set-property 'Titanium:RightButton4 ':after-set rebuild-theme)
)
