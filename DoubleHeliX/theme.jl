;; theme file, written Wed Mar  8 09:30:29 2000
;; created by sawmill-themer -- DO NOT EDIT!

(require 'make-theme)

(let
    ((patterns-alist
      '(("part#12"
         (inactive
          "part#12-inactive.png")
         (focused
          "part#12.png"))
        ("part#11"
         (inactive
          "part#11-inactive.png")
         (focused
          "part#11.png"))
        ("part#10"
         (inactive
          "part#10-inactive.png")
         (focused
          "part#10.png"))
        ("part#9"
         (inactive
          "part#9-inactive.png")
         (focused
          "part#9.png")
         (highlighted
          "part#9.png"))
        ("part#8"
         (inactive
          "part#8-inactive.png")
         (focused
          "part#8.png")
         (highlighted
          "part#8.png")
         (clicked
          "part#8.png"))
        ("part#7"
         (inactive
          "part#7-inactive.png")
         (focused
          "part#7.png")
         (clicked
          "part#7-clicked.png"))
        ("part#6"
         (inactive
          "part#6-inactive.png")
         (focused
          "part#6.png")
         (clicked
          "part#6-clicked.png"))
        ("part#5"
         (inactive
          "part#5-inactive.png")
         (focused
          "part#5.png")
         (clicked
          "part#5-clicked.png"))
        ("part#4"
         (inactive
          "part#4-inactive.png")
         (focused
          "part#4.png"))
        ("part#3"
         (inactive
          "part#3-inactive.png"
	  (border
	  38
	  38
	  0
	  0)
	  )
         (focused
          "part#3-hi.png"
          (border
           38
           38
           0
           0)))
        ("part#2"
         (inactive
          "part#2-inactive.png")
         (focused
          "part#2.png"))
        ("part"
         (inactive
          "part-inactive.png")
         (focused
          "part.png")
         (clicked
          "part-clicked.png"))
        ("title-colors"
         (inactive . "#6e146e146e14")
         (focused . "#000000000000"))
        ("bottomleft-shaped"
         (inactive
          "bl-shaped-u.png")
         (focused
          "bl-shaped.png"))
        ("bottomright-shaped"
         (inactive
          "br-shaped-u.png")
         (focused
          "br-shaped.png"))
        ("top-border-only"
         (inactive
          "top-border-only-inactive.png"
          (border
           6
           6
           2
           1))
         (focused
          "top-border-only.png"
          (border
           6
           6
           2
           1)))))

     (frames-alist
      '(("normal"
         ((right-edge . -6)
          (top-edge . 0)
          (background . "part#12")
          (bottom-edge . 0)
          (class . right-border)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#11")
          (right-edge . -6)
          (bottom-edge . -6)
          (class . bottom-right-corner)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#10")
          (left-edge . 0)
          (right-edge . 0)
          (bottom-edge . -6)
          (class . bottom-border)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#9")
          (left-edge . -6)
          (bottom-edge . -6)
          (class . bottom-left-corner)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#8")
          (left-edge . -6)
          (top-edge . 0)
          (bottom-edge . 0)
          (class . left-border)
          (x-justify . center)
          (y-justify . center))
         ((right-edge . -6)
          (background . "part#7")
          (top-edge . -22)
          (class . close-button)
          (x-justify . center)
          (y-justify . center))
         ((right-edge . 14)
          (background . "part#6")
          (top-edge . -22)
          (class . maximize-button)
          (x-justify . center)
          (y-justify . center))
         ((top-edge . -22)
          (right-edge . 30)
          (background . "part#5")
          (class . iconify-button)
          (x-justify . center)
          (y-justify . center))
         ((top-edge . -22)
          (right-edge . 48)
          (background . "part#4")
          (class . title)
          (x-justify . center)
          (y-justify . center))
         ((left-edge . 18)
          (top-edge . -22)
          (font . "-adobe-helvetica-medium-r-normal-*-*-120-*-*-p-*-iso8859-1")
          (right-edge . 50)
          (foreground . "title-colors")
          (background . "part#3")
          (class . title)
          (text . window-name)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#2")
          (left-edge . 16)
          (top-edge . -22)
          (class . title))
         ((left-edge . -6)
          (background . "part")
          (top-edge . -22)
          (class . menu-button)))
        ("shaped"
         ((right-edge . -6)
          (top-edge . -22)
          (background . "part#7")
          (class . close-button)
          (x-justify . center)
          (y-justify . center))
         ((top-edge . -22)
          (right-edge . 14)
          (background . "part#6")
          (class . maximize-button)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#5")
          (right-edge . 30)
          (top-edge . -22)
          (class . iconify-button)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#4")
          (right-edge . 48)
          (top-edge . -22)
          (class . title)
          (x-justify . center)
          (y-justify . center))
         ((right-edge . 50)
          (font . "-adobe-helvetica-medium-r-normal-*-*-120-*-*-p-*-iso8859-1")
          (top-edge . -22)
          (left-edge . 18)
          (foreground . "title-colors")
          (background . "part#3")
          (class . title)
          (text . window-name)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#2")
          (left-edge . 16)
          (top-edge . -22)
          (class . title))
         ((background . "part")
          (left-edge . -6)
          (top-edge . -22)
          (class . menu-button))
         ((left-edge . -6)
          (top-edge . -3)
          (class . title)
          (background . "bottomleft-shaped"))
         ((top-edge . -3)
          (right-edge . -6)
          (class . title)
          (background . "bottomright-shaped")))
        ("transient"
         ((top-edge . 0)
          (right-edge . -6)
          (background . "part#12")
          (bottom-edge . 0)
          (class . right-border)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#11")
          (right-edge . -6)
          (bottom-edge . -6)
          (class . bottom-right-corner)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#10")
          (left-edge . 0)
          (right-edge . 0)
          (bottom-edge . -6)
          (class . bottom-border)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#9")
          (left-edge . -6)
          (bottom-edge . -6)
          (class . bottom-left-corner)
          (x-justify . center)
          (y-justify . center))
         ((top-edge . 0)
          (left-edge . -6)
          (background . "part#8")
          (bottom-edge . 0)
          (class . left-border)
          (x-justify . center)
          (y-justify . center))
         ((background . "top-border-only")
          (top-edge . -6)
          (right-edge . -6)
          (left-edge . -6)
          (class . title)))
        ("shaped-transient"
         ((right-edge . -6)
          (top-edge . -22)
          (background . "part#7")
          (class . close-button)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#6")
          (right-edge . 14)
          (top-edge . -22)
          (class . maximize-button)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#5")
          (right-edge . 30)
          (top-edge . -22)
          (class . iconify-button)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#4")
          (right-edge . 48)
          (top-edge . -22)
          (class . title)
          (x-justify . center)
          (y-justify . center))
         ((font . "-adobe-helvetica-medium-r-normal-*-*-120-*-*-p-*-iso8859-1")
          (right-edge . 50)
          (foreground . "title-colors")
          (background . "part#3")
          (left-edge . 18)
          (top-edge . -22)
          (class . title)
          (text . window-name)
          (x-justify . center)
          (y-justify . center))
         ((background . "part#2")
          (left-edge . 16)
          (top-edge . -22)
          (class . title))
         ((background . "part")
          (left-edge . -6)
          (top-edge . -22)
          (class . menu-button)))
        ("unframed")))

     (mapping-alist
      '((default . "normal")
        (transient . "transient")
        (shaped . "shaped")
        (shaped-transient . "shaped-transient")
        (unframed . "nil")))

     (theme-name 'DoubleHeliX))

  (add-frame-style
   theme-name (make-theme patterns-alist frames-alist mapping-alist))
  (when (boundp 'mark-frame-style-editable)
    (mark-frame-style-editable theme-name)))

