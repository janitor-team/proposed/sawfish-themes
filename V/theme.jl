;;  -V- theme for Sawfish by Gr1dl0ck (gr1dl0ck.deviantart.com)
;;   v0.3
;
; This file is part of the -V- Sawfish theme.
;
; -V- is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
;
; -V- is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this theme; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


(let*
    ;; 13x11
    ((menu-images (list (make-image "menu-unpressed.png")
			(make-image "menu-unpressed.png")
			(make-image "menu-pressed.png")
                        (make-image "menu-dark.png")))

     (iconify-images (list (make-image "iconify-unpressed.png")
			   (make-image "iconify-unpressed.png")
 			   (make-image "iconify-pressed.png")
			   (make-image "iconify-dark.png")))

     ;;41x19
     (top-left-corner-image (make-image "top-left-corner.png"))
     (top-left-corner-shaded-image (make-image "top-left-corner-shaded.png"))
     
     (title-left-images (list (make-image "inact-title-left.png")
                              (make-image "act-title-left.png")))

     (title-images (list (make-image "inact-title.png")
                         (make-image "act-title.png")))

     (top-right-corner-images (list (make-image "inact-top-right-corner.png")
                                    (make-image "act-top-right-corner.png")))
     (top-right-corner-shaded-images (list (make-image "inact-top-right-corner-shaded.png")     
                                      (make-image "act-top-right-corner-shaded.png")))	

     ;;4x26
     (left-border-nostretch-image (make-image "left-border-nostretch.png"))
     ;;4x18
     (left-border-stretch-image (make-image "left-border-stretch.png"))
     ;;7x6
     (bottom-left-corner-image (make-image "bottom-left-corner.png"))
	
     (right-border-image (make-image "right-border.png"))
     (bottom-right-corner-image (make-image "bottom-right-corner.png"))

     (bottom-border-nostretch-image (make-image "bottom-border-nostretch.png"))
     (bottom-border-stretch-image (make-image "bottom-border-stretch.png"))


	;; Transient Window Graphics ::
     (t-top-left-corner-images (list (make-image "t-inact-top-left-corner.png")
				    (make-image "t-act-top-left-corner.png")))

     (t-menu-images (list (make-image "t-unpressed-menu.png")
                          (make-image "t-unpressed-menu.png") nil
  		          (make-image "t-pressed-menu.png")))

     (t-left-bar-images (list (make-image "t-inact-left-bar.png")
			     (make-image "t-act-left-bar.png")))

     (t-bottom-left-corner-images (list (make-image "t-inact-bottom-left-corner.png")
                                        (make-image "t-act-bottom-left-corner.png")))

     (t-top-border-image (make-image "t-top-border.png"))

     (t-top-right-corner-image (make-image "t-top-right-corner.png"))

     (t-bottom-border-image (make-image "t-bottom-border.png"))

     (t-bottom-right-corner-image (make-image "t-bottom-right-corner.png"))

     (t-right-border-image (make-image "t-right-border.png"))

     ;; frame layout 

     (frame `(((background . ,top-left-corner-image)
	       (top-edge . -19)
	       (left-edge . -4)
	       (class . top-left-corner))

	      ((background . ,menu-images)
               (top-edge . -19)
	       (left-edge . 5)
               (class . menu-button))

	      ((background . ,iconify-images)
               (top-edge . -19)
	       (left-edge . 21)
               (class . iconify-button))
		;;titlebar
              ((background . ,title-left-images)
               (top-edge . -19)
	       (left-edge . 33)
	       (class . title))

              ((background . ,title-images)
               (foreground . "black")
               (font . "-adobe-helvetica-bold-r-normal-*-*-100-*-*-p-*-iso8859-1")
               (text . ,window-name)
               (x-justify . 4)
               (y-justify . center)
	       (top-edge . -19)
               (left-edge . 48)
	       (right-edge . 24)
               (class . title))

              ((background . ,top-right-corner-images)
               (top-edge . -19)
               (right-edge . -4)
               (class . title))	      

		;;left border
 	      ((background . ,left-border-nostretch-image)
               (top-edge . 0)
	       (left-edge . -4)
	       (class . left-border))

	      ((background . ,left-border-stretch-image)
               (top-edge . 26)
               (left-edge . -4)
               (bottom-edge . -3)
               (class . left-border))

	      ((background . ,bottom-left-corner-image)
	       (bottom-edge . -4)
	       (left-edge . -4)
	       (class . bottom-left-corner))

		;;;right border
	      ((background . ,right-border-image)
	       (right-edge . -4)
	       (top-edge . 0)
	       (bottom-edge . 3)
               (class . right-border))

	      ((background . ,bottom-right-corner-image)
               (right-edge . -4)
	       (bottom-edge . -4)
               (class . bottom-right-corner))

		;;;bottom-border
	      ((background . ,bottom-border-nostretch-image)
               (right-edge . 0)
               (bottom-edge . -4)
               (class . bottom-border))

	      ((background . ,bottom-border-stretch-image)
               (right-edge . 39)
               (left-edge . 3)
               (bottom-edge . -4)
	       (class . bottom-border))))

     (shaped-frame `(((background . ,top-left-corner-shaded-image)
                      (top-edge . -19)
                      (left-edge . -4)
                      (class . top-left-corner))

                     ((background . ,menu-images)
                      (top-edge . -19)
                      (left-edge . 5)
                      (class . menu-button))

                     ((background . ,iconify-images)
                      (top-edge . -19)
                      (left-edge . 21)
                      (class . iconify-button))
                      ;;titlebar
                     ((background . ,title-left-images)
                      (top-edge . -19)
                      (left-edge . 33)
                      (class . title))

                     ((background . ,title-images)
                      (foreground . "black")
                      (font . "-adobe-helvetica-bold-r-normal-*-*-100-*-*-p-*-iso8859-1")
                      (text . ,window-name)
                      (x-justify . 4)
                      (y-justify . center)
                      (top-edge . -19)
                      (left-edge . 48)
                      (right-edge . 24)
                      (class . title))

                     ((background . ,top-right-corner-shaded-images)
                      (top-edge . -19)
                      (right-edge . -4)
                      (class . title))))

     (transient-frame `(((background . ,t-top-left-corner-images)
                         (top-edge . -4)
                         (left-edge . -11)
		  	 (class . title))

			((background . ,t-menu-images)
			 (top-edge . 6)
			 (left-edge . -11)
		  	 (class . menu-button))  
 
			((background . ,t-left-bar-images)
			 (top-edge . 42)
		         (left-edge . -11)
			 (bottom-edge . 26)
			 (class . title))
	
			((background . ,t-bottom-left-corner-images)
			 (bottom-edge . -4)
			 (left-edge . -11)
			 (class . title))

			((background . ,t-top-border-image)
	    		 (top-edge . -4)
			 (left-edge . 5)
			 (right-edge . 17))
	       
			((background . ,t-top-right-corner-image)
			 (top-edge . -4)
			 (right-edge . -4))

			((background . ,t-right-border-image)
			 (top-edge . 1)
			 (right-edge . -4)
			 (bottom-edge . 7))
	
			((background . ,t-bottom-right-corner-image)
			 (bottom-edge . -4)
			 (right-edge . -4))

			((background . ,t-bottom-border-image)
			 (left-edge . 3)
			 (right-edge . 2)
			 (bottom-edge . -4))))

     (shaped-transient-frame `(((background . ,t-top-left-corner-images)
                         (top-edge . -4)
                         (left-edge . -11)
                         (class . title))

                        ((background . ,t-menu-images)
                         (top-edge . 6)
                         (left-edge . -11)
                         (class . menu-button))

                        ((background . ,t-left-bar-images)
                         (top-edge . 42)
                         (left-edge . -11)
                         (bottom-edge . 26)
                         (class . title))

                        ((background . ,t-bottom-left-corner-images)
                         (bottom-edge . -4)
                         (left-edge . -11)
                         (class . title))))

)

 (add-frame-style 'V
                   (lambda (w type)
                     (case type
                       ((default) frame) 
                       ((transient) frame)
                       ((shaped) shaped-frame)
                       ((shaped-transient) shaped-frame)))))
