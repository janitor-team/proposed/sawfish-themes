;; theme file, written Mon Aug 19 14:03:46 2002
;; created by sawfish-themer -- DO NOT EDIT!

(require 'make-theme)

(let
    ((patterns-alist
      '(("close-button"
         (inactive
          "close-up.png"
          (border
           0
           0
           0
           0))
         (clicked
          "close-down.png"))
        ("maximize-button"
         (inactive
          "restore-up.png")
         (clicked
          "maximize-down.png"))
        ("minimize-button"
         (inactive
          "minimize-up.png")
         (clicked
          "minimize-down.png"))
        ("controlbox"
         (inactive
          "menu-up.png")
         (clicked
          "menu-down.png"))
        ("title-bar"
         (inactive
          "unfocus-titlebar-up.png")
         (focused
          "titlebar.png")
         (clicked
          "titlebar-down.png"))
        ("text-color"
         (inactive . "#c5d0c5d0c5d0")
         (focused . "#ffffffffffff"))
        ("h-frame"
         (inactive
          "hborder.png"))
        ("v-frame"
         (inactive
          "vborder.png"))
        ("botright-corner"
         (inactive
          "botright.png"))
        ("botleft-corner"
         (inactive
          "boleft.png"))))

     (frames-alist
      '(("default"
         ((right-edge . -8)
          (top-edge . -40)
          (background . "close-button")
          (class . close-button))
         ((top-edge . -40)
          (right-edge . 32)
          (background . "maximize-button")
          (class . maximize-button))
         ((top-edge . -40)
          (right-edge . 72)
          (background . "minimize-button")
          (class . iconify-button))
         ((font . "-b&h-lucida console-medium-r-normal-*-*-160-*-*-m-*-koi8-u")
          (left-edge . 32)
          (right-edge . 112)
          (top-edge . -40)
          (text . window-name)
          (background . "title-bar")
          (foreground . "text-color")
          (y-justify . center)
          (x-justify . 10)
          (class . title))
         ((top-edge . -40)
          (left-edge . -8)
          (background . "controlbox")
          (class . menu-button))
         ((right-edge . 0)
          (height . 8)
          (bottom-edge . -8)
          (left-edge . 0)
          (background . "h-frame")
          (class . bottom-border))
         ((left-edge . -8)
          (bottom-edge . 0)
          (top-edge . 0)
          (background . "v-frame")
          (class . left-border))
         ((right-edge . -8)
          (top-edge . 0)
          (bottom-edge . 0)
          (background . "v-frame")
          (class . right-border))
         ((bottom-edge . -8)
          (right-edge . -8)
          (background . "botright-corner")
          (class . bottom-right-corner))
         ((bottom-edge . -8)
          (background . "botleft-corner")
          (left-edge . -8)
          (class . bottom-left-corner)))))

     (mapping-alist
      '((default . "default")
        (transient . "default")
        (shaped . "default")
        (shaped-transient . "default")
        (unframed . "default")))

     (theme-name 'Big))

  (add-frame-style
   theme-name (make-theme patterns-alist frames-alist mapping-alist))
  (when (boundp 'mark-frame-style-editable)
    (mark-frame-style-editable theme-name)))
