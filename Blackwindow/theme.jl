;; Blackwindow 
;; A blackletter theme.
;; by Matt Chisholm http://www.theory.org/~matt/sawfish/

(let*
  (
    ;; Update window title pixel length
    (title-width
      (lambda (w)
        (let
          ((w-width (car (window-dimensions w))))
          (max 0 (min (- w-width 100) (text-width (window-name w) 
          ))))))

     (thumb-half-width
      (lambda (w)
        (/ (- (car (window-dimensions w)) (title-width w)) 2)))

     (half-width 
      (lambda (w)
        (/ (car (window-dimensions w)) 2)))

     (half-height 
      (lambda (w)
        (/ (cdr (window-dimensions w)) 2)))

        (topleft (list 
         (make-image "top-left-i.png")
         (make-image "top-left-a.png")))
	(title-r (list
	 (make-image "title-r-i.png")
	 (make-image "title-r-a.png")))
	(title-l (list
	 (make-image "title-l-i.png")
	 (make-image "title-l-a.png")))
	(title (list
	 (make-image "title-i.png")
	 (make-image "title-a.png")))
        (top (list 
         (make-image "top-i.png")
         (make-image "top-a.png")))
        (topright (list 
         (make-image "top-right-i.png")
         (make-image "top-right-a.png")))
        (right (list 
         (make-image "side-i.png")
         (make-image "side-a.png")))
        (right-d (list 
         (make-image "right-i.png")
         (make-image "right-a.png")))
        (bottomright (list 
         (make-image "bottom-right-i.png")
         (make-image "bottom-right-a.png")))
        (bottom (list 
         (make-image "bottom-i.png")
         (make-image "bottom-a.png")))
        (bottomleft (list 
         (make-image "bottom-left-i.png")
         (make-image "bottom-left-a.png")))
        (left (list 
         (make-image "side-i.png")
         (make-image "side-a.png"))) 
        (left-d (list 
         (make-image "left-i.png")
         (make-image "left-a.png"))) 

	(button-1 (list
	  (make-image "button-1-i.png")
	  (make-image "button-1-a.png")
	  (make-image "button-1-h.png")
	  (make-image "button-1-a.png")))
	(button-4 (list
	  (make-image "button-4-i.png")
	  (make-image "button-4-a.png")
	  (make-image "button-4-h.png")
	  (make-image "button-4-a.png")))
	(button-3 (list
	  (make-image "button-3-i.png")
	  (make-image "button-3-a.png")
	  (make-image "button-3-h.png")
	  (make-image "button-3-a.png")))
	(button-2 (list
	  (make-image "button-2-i.png")
	  (make-image "button-2-a.png")
	  (make-image "button-2-h.png")
	  (make-image "button-2-a.png")))

	(bottom-d (list
	  (make-image "bottom-d-i.png")
	  (make-image "bottom-d-a.png")))

	(title-r-s (list
	 (make-image "title-r-i-s.png")
	 (make-image "title-r-a-s.png")))
	(title-l-s (list
	 (make-image "title-l-i-s.png")
	 (make-image "title-l-a-s.png")))
	(title-s (list
	 (make-image "title-i.png")
	 (make-image "title-a.png")))

	;; number of pixels with which to pad the title
	(title-pad 10)

	(font-colors ( list "#2f2f2f" "#1f1f1f" "#000000" "#2f2f2f" ))
	(font-colors2 ( list "#ff0000" ))
;;	(font2 (get-font "-sharefont-blackforest-normal-r-normal-*-*-180-*-*-p-*-iso8859-1"))

       (frame `(
	;; corners
         ((class . top-left-corner)
	  (top-edge . -17)
          (left-edge . -23)
          (background . ,topleft))
         ((bottom-edge . -32)
          (left-edge . -21)
          (background . ,bottomleft)
          (class . bottom-left-corner))
         ((top-edge . -17)
          (background . ,topright)
          (right-edge . -23)
          (class . top-right-corner))
         ((right-edge . -21)
          (background . ,bottomright)
          (bottom-edge . -32)
          (class . bottom-right-corner))

	;; edges
         ((left-edge . 4)
          (right-edge . 4)
          (top-edge . -3)
          (background . ,top)
          (class . top-border))
         ((top-edge . 39)
          (bottom-edge . 15)
          (left-edge . -6)
          (background . ,left)
          (class . left-border))
         ((left-edge . 1)
          (right-edge . 1)
          (bottom-edge . -3)
          (background . ,bottom)
          (class . bottom-border))
         ((top-edge . 39)
          (right-edge . -6)
          (bottom-edge . 15)
          (background . ,right)
          (class . right-border))

	;; side decorations
	 ((class . right-border)
	  (right-edge . -10)
	  (top-edge . ,(lambda (w) (half-height w)))
	  (background . ,right-d))
	 ((class . left-border)
	  (left-edge . -10)
	  (top-edge . ,(lambda (w) (half-height w)))
	  (background . ,left-d))
	;; bottom decoration
	 ((class . bottom-border)
	  (background . ,bottom-d)
	  (bottom-edge . -19)
	  (left-edge . ,(lambda (w) (- (half-width w) 6)))
	  (right-edge . ,(lambda (w) (- (half-width w) 6))))

	;; title bookends
	 ((class . title)
	  (right-edge . ,(lambda (w) (- (thumb-half-width w) title-pad 26)))
	  (top-edge . -40)
	  (background . ,title-r))
	 ((class . title)
	  (left-edge . ,(lambda (w) (- (thumb-half-width w) title-pad 26)))
	  (top-edge . -40)
	  (background . ,title-l))

	;; title
         ((left-edge . ,(lambda (w) (- (thumb-half-width w) title-pad)))
	  (right-edge . ,(lambda (w) (- (thumb-half-width w) title-pad)))
          (top-edge . -33)
          (width . ,(lambda (w) (+ (title-width w) title-pad title-pad )))
          (y-justify . center)
          (x-justify . center)
          (text . ,window-name)
	  (foreground . ,font-colors)
	  (font . ,font)
          (background . ,title)
          (class . title))

	;; side buttons
	 ((class . close-button)
	  (left-edge . -16)
	  (top-edge . 4)
	  (background . ,button-1))
	 ((class . maximize-button)
	  (right-edge . -16)
	  (top-edge . 4)
	  (background . ,button-4))

	;; top buttons
	 ((class . iconify-button)
	  (right-edge . ,(lambda (w) (- (thumb-half-width w) title-pad 21)))
	  (top-edge . -21)
	  (background . ,button-3))
	 ((class . shade-button)
	  (left-edge . ,(lambda (w) (- (thumb-half-width w) title-pad 21)))
	  (top-edge . -21)
	  (background . ,button-2))
    ))

	;; title bookends
       (shaped-frame `(
	 ((class . title)
	  (right-edge . ,(lambda (w) (- (thumb-half-width w) title-pad 26)))
	  (top-edge . -40)
	  (background . ,title-r-s))
	 ((class . title)
	  (left-edge . ,(lambda (w) (- (thumb-half-width w) title-pad 26)))
	  (top-edge . -40)
	  (background . ,title-l-s))

	;; title
         (
	  (left-edge . ,(lambda (w) (- (thumb-half-width w) title-pad)))
	  (right-edge . ,(lambda (w) (- (thumb-half-width w) title-pad)))
          (top-edge . -33)
          (width . ,(lambda (w) (+ (title-width w) 12 )))
          (y-justify . center)
          (x-justify . center)
          (text . ,window-name)
	  (foreground . ,font-colors)
	  (font . ,font)
          (background . ,title)
          (class . title))

	;; title buttons
	 ((class . iconify-button)
	  (right-edge . ,(lambda (w) (- (thumb-half-width w) title-pad 21)))
	  (top-edge . -21)
	  (background . ,button-3))
	 ((class . shade-button)
	  (left-edge . ,(lambda (w) (- (thumb-half-width w) title-pad 21)))
	  (top-edge . -21)
	  (background . ,button-2))
    ))
)

  (add-frame-style 'Blackwindow
                   (lambda (w type)
                     (case type
                       ((default) frame)
                       ((transient) frame)
                       ((shaped) shaped-frame)
                       ((shaped-transient) shaped-frame))))

  (call-after-property-changed
   'WM_NAME (lambda ()
              (rebuild-frames-with-style 'Blackwindow))))
