;; theme file, written Mon May 20 19:12:29 2002
;; created by sawfish-themer -- DO NOT EDIT!


;; lower button definition

(def-frame-class lower-button '()
    (bind-keys lower-button-keymap "Button1-Off" 'lower-window))

;; sawfish-themer stuff

(require 'make-theme)

(let
    ((patterns-alist
      '(("title2"
         (inactive
          "title2_normal.png"
          (border
           35
           5
           0
           0))
         (focused
          "title2_active.png"
          (border
           35
           5
           0
           0)))
        ("kill"
         (inactive
          "kill_normal.png")
         (focused
          "kill_active.png")
         (clicked
          "kill_clicked.png"))
        ("side_left"
         (inactive
          "side_left_normal.png"
          (border
           0
           0
           2
           2))
         (focused
          "side_left_active.png"
          (border
           0
           0
           2
           2)))
        ("side_right"
         (inactive
          "side_right.png"
          (border
           0
           0
           2
           2))
         (focused
          "side_right.png"
          (border
           0
           0
           2
           2)))
        ("bottom"
         (inactive
          "bottom.png"
          (border
           2
           2
           0
           0))
         (focused
          "bottom.png"
          (border
           2
           2
           0
           0)))
        ("bottom_left_corner"
         (inactive
          "bottom_left_corner.png")
         (focused
          "bottom_left_corner.png"))
        ("bottom_right_corner"
         (inactive
          "bottom_right_corner_normal.png")
         (focused
          "bottom_right_corner_active.png"))
        ("iconify"
         (inactive
          "iconify_normal.png")
         (focused
          "iconify_active.png")
         (clicked
          "iconify_clicked.png"))
        ("maximise"
         (inactive
          "maximise_normal.png")
         (focused
          "maximise_active.png")
         (clicked
          "maximise_clicked.png"))
        ("lower"
         (inactive
          "lower_normal.png")
         (focused
          "lower_active.png")
         (clicked
          "lower_clicked.png"))
        ("base"
         (inactive
          "base.png"
          (border
           2
           3
           2
           2))
         (focused
          "base.png"))))

     (frames-alist
      '(("default"
         ((font . "-*-infernal-bold-r-normal-*-*-120-*-*-p-*-*-*")
          (y-justify . 1)
          (x-justify . center)
          (left-edge . -5)
          (top-edge . -21)
          (right-edge . 48)
          (background . "title2")
          (text . window-name)
          (class . title))
         ((top-edge . -13)
          (left-edge . 6)
          (background . "kill")
          (foreground . "kill")
          (class . close-button))
         ((below-client . t)
          (left-edge . -5)
          (right-edge . -5)
          (top-edge . -18)
          (background . "base")
          (class . top-border))
         ((bottom-edge . 9)
          (top-edge . -17)
          (background . "side_left")
          (left-edge . -5)
          (class . left-border))
         ((bottom-edge . 9)
          (top-edge . -2)
          (background . "side_right")
          (right-edge . -5)
          (class . right-border))
         ((left-edge . 0)
          (right-edge . 0)
          (background . "bottom")
          (bottom-edge . -5)
          (class . bottom-border))
         ((left-edge . -5)
          (bottom-edge . -5)
          (background . "bottom_left_corner")
          (class . bottom-left-corner))
         ((right-edge . -5)
          (background . "bottom_right_corner")
          (bottom-edge . -5)
          (class . bottom-right-corner))
         ((background . "lower")
          (top-edge . -16)
          (right-edge . 0)
          (class . lower-button))
         ((right-edge . 16)
          (background . "maximise")
          (top-edge . -16)
          (class . maximize-button))
         ((right-edge . 32)
          (background . "iconify")
          (top-edge . -16)
          (class . iconify-button)))
        ("shaded"
         ((right-edge . 48)
          (top-edge . -21)
          (font . "-*-infernal-bold-r-normal-*-*-120-*-*-p-*-*-*")
          (y-justify . 1)
          (x-justify . center)
          (left-edge . -5)
          (background . "title2")
          (text . window-name)
          (class . title))
         ((top-edge . -13)
          (left-edge . 6)
          (background . "kill")
          (foreground . "kill")
          (class . close-button))
         ((below-client . t)
          (left-edge . -5)
          (right-edge . -5)
          (top-edge . -18)
          (background . "base")
          (class . top-border))
         ((right-edge . 0)
          (top-edge . -16)
          (background . "lower")
          (class . lower-button))
         ((right-edge . 16)
          (background . "maximise")
          (top-edge . -16)
          (class . maximize-button))
         ((right-edge . 32)
          (background . "iconify")
          (top-edge . -16)
          (class . iconify-button)))))

     (mapping-alist
      '((default . "default")
        (shaped . "shaded")
        (transient . "default")
        (shaped-transient . "shaded")))

     (theme-name 'OrangeJuice))

  (add-frame-style
   theme-name (make-theme patterns-alist frames-alist mapping-alist))
  (when (boundp 'mark-frame-style-editable)
    (mark-frame-style-editable theme-name)))
