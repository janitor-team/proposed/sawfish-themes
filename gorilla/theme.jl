;; theme file, written Thu Dec  6 01:47:21 2001
;; created by sawfish-themer -- DO NOT EDIT!

(require 'make-theme)

(let
    ((patterns-alist
      '(("title-colors"
         (inactive . "#8c5f8e8c9554")
         (focused . "#ffffffffffff"))
        ("left-border"
         (inactive
          "left-u.png")
         (focused
          "left.png"))
        ("right-border"
         (inactive
          "right-u.png")
         (focused
          "right.png"))
        ("bottom-border"
         (inactive
          "bottom-u.png")
         (focused
          "bottom.png"))
        ("top-border"
         (inactive
          "top-u.png")
         (focused
          "top.png"))
        ("tl"
         (inactive
          "tl-u.png")
         (focused
          "tl.png"))
        ("tr"
         (inactive
          "tr-u.png")
         (focused
          "tr.png"))
        ("bl"
         (inactive
          "bl-u.png")
         (focused
          "bl.png"))
        ("br"
         (inactive
          "br-u.png")
         (focused
          "br.png"))
        ("title"
         (inactive . "#59995c286666")
         (focused
          "title.png"))
        ("min"
         (inactive
          "min-unfocused.png")
         (focused
          "min.png"))
        ("max"
         (inactive
          "max-unfocused.png")
         (focused
          "max.png"))
        ("menu"
         (inactive
          "menu-unfocused.png")
         (focused
          "menu.png"))))

     (frames-alist
      '(("default-frame"
         ((top-edge . -17)
          (right-edge . 0)
          (y-justify . 2)
          (x-justify . 8)
          (text . window-name)
          (foreground . "title-colors")
          (left-edge . 0)
          (background . "title")
          (class . title))
         ((left-edge . -6)
          (top-edge . -15)
          (bottom-edge . 0)
          (background . "left-border")
          (class . left-border))
         ((right-edge . -6)
          (top-edge . -15)
          (bottom-edge . 0)
          (background . "right-border")
          (class . right-border))
         ((top-edge . -23)
          (right-edge . 0)
          (left-edge . 0)
          (background . "top-border")
          (class . top-border))
         ((bottom-edge . -6)
          (right-edge . 0)
          (left-edge . 0)
          (background . "bottom-border")
          (class . bottom-border))
         ((left-edge . -6)
          (top-edge . -23)
          (background . "tl")
          (class . top-left-corner))
         ((right-edge . -6)
          (background . "tr")
          (top-edge . -23)
          (class . top-right-corner))
         ((left-edge . -6)
          (bottom-edge . -6)
          (background . "bl")
          (class . bottom-left-corner))
         ((background . "br")
          (bottom-edge . -6)
          (right-edge . -6)
          (class . bottom-right-corner))
         ((top-edge . -14)
          (right-edge . 4)
          (background . "menu")
          (class . close-button))
         ((right-edge . 22)
          (top-edge . -14)
          (background . "max")
          (class . maximize-button))
         ((right-edge . 40)
          (top-edge . -14)
          (background . "min")
          (class . iconify-button)))
        ("shaded"
         ((top-edge . -17)
          (right-edge . 0)
          (y-justify . 2)
          (x-justify . 8)
          (text . window-name)
          (foreground . "title-colors")
          (left-edge . 0)
          (background . "title")
          (class . title))
         ((left-edge . 0)
          (top-edge . -23)
          (right-edge . 0)
          (background . "top-border")
          (class . top-border))
         ((top-edge . -23)
          (left-edge . -6)
          (background . "tl")
          (class . top-left-corner))
         ((top-edge . -23)
          (right-edge . -6)
          (background . "tr")
          (class . top-right-corner))
         ((top-edge . -14)
          (right-edge . 4)
          (background . "menu")
          (class . close-button)))
        ("transient"
         ((top-edge . -17)
          (right-edge . 0)
          (y-justify . 2)
          (x-justify . 8)
          (text . window-name)
          (foreground . "title-colors")
          (left-edge . 0)
          (background . "title")
          (class . title))
         ((left-edge . -6)
          (top-edge . -15)
          (bottom-edge . 0)
          (background . "left-border")
          (class . left-border))
         ((right-edge . -6)
          (top-edge . -15)
          (bottom-edge . 0)
          (background . "right-border")
          (class . right-border))
         ((top-edge . -23)
          (right-edge . 0)
          (left-edge . 0)
          (background . "top-border")
          (class . top-border))
         ((bottom-edge . -6)
          (right-edge . 0)
          (left-edge . 0)
          (background . "bottom-border")
          (class . bottom-border))
         ((left-edge . -6)
          (top-edge . -23)
          (background . "tl")
          (class . top-left-corner))
         ((right-edge . -6)
          (background . "tr")
          (top-edge . -23)
          (class . top-right-corner))
         ((left-edge . -6)
          (bottom-edge . -6)
          (background . "bl")
          (class . bottom-left-corner))
         ((background . "br")
          (bottom-edge . -6)
          (right-edge . -6)
          (class . bottom-right-corner)))
        ("shaded-transient"
         ((top-edge . -17)
          (right-edge . 0)
          (y-justify . 2)
          (x-justify . 8)
          (text . window-name)
          (foreground . "title-colors")
          (left-edge . 0)
          (background . "title")
          (class . title))
         ((top-edge . -23)
          (right-edge . 0)
          (left-edge . 0)
          (background . "top-border")
          (class . top-border))
         ((top-edge . -23)
          (left-edge . -6)
          (background . "tl")
          (class . top-left-corner))
         ((top-edge . -23)
          (right-edge . -6)
          (background . "tr")
          (class . top-right-corner)))))

     (mapping-alist
      '((default . "default-frame")
        (shaded . "shaded")
        (shaded-transient . "shaded-transient")
        (transient . "transient")))

     (theme-name 'gorilla))

  (add-frame-style
   theme-name (make-theme patterns-alist frames-alist mapping-alist))
  (when (boundp 'mark-frame-style-editable)
    (mark-frame-style-editable theme-name)))
